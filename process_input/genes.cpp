
#include <string>
#include <vector>
#include <algorithm>
#include <random>
#include <chrono>
#include <fstream>
#ifdef USE_EXPERIMENTAL_FILESYSTEM
#include <experimental/filesystem>
#else
#include <filesystem>
#endif
#include <algorithm>
#include "genes.h"

#ifdef USE_EXPERIMENTAL_FILESYSTEM
namespace fs = std::experimental::filesystem;
#else
namespace fs = std::filesystem;
#endif


void Genes::shuffleMe() {

}

void Genes::readFastas(std::string directory) {

    for (const auto & entry : fs::directory_iterator(directory)) {
        if(fs::is_directory(entry.path()) ) {
          std::cerr << "\33[2K\rreading genes of " << entry.path().filename().string() << std::flush; // white space to write over longer names
          std::string fastafile = "";
          for (const auto & file : fs::directory_iterator(entry)) {
              if(fs::is_regular_file(file.path()) && file.path().extension() == ".fasta") {
                  // std::cerr << "fasta file: " << file.path() << std::endl;
                  // readFasta("bdi", "/data/bls/newdata/nextflow_output/bdi/masked_promoter_bdi.fasta");
                  if (fastafile.empty()) fastafile = file.path().string();
                  else {
                      if(file.path().string().find("geneid") != std::string::npos)
                        fastafile = file.path().string();
                  }
              }
          }
          readFasta(entry.path().filename(), fastafile);
        }
        // std::cerr << "genemap size: " << genemap.size() << std::endl;
    }
    // for (auto x : genemap) {
        // std::cerr << x.first << " -> " << x.second << std::endl;
    // }
    std::cerr << '\r' << genemap.size() << " genes in genemap" << std::endl;
}
Gene *Genes::getGene(std::string geneid) {
    if(genemap.find(geneid) != genemap.end())
        return &genemap[geneid];
    // else
        // std::cerr << "geneid " << geneid << " not found in genemap" << std::endl;
    return NULL;
}
char Genes::getRandomCharFromIupac(char c) {
    char new_c = characterToMask[c][rand() % characterToMask[c].size()];
    // std::cerr << "replaced " << c << " with " << new_c << std::endl;
    return new_c;
}
void Genes::fixSequence(std::string &seq) {
    std::for_each(seq.begin(), seq.end(), [](char & c) {
        if (replaceIupacCharacters.find(c) != validCharacters.end()) {
            c = getRandomCharFromIupac(c);
        }
    });
}
void Genes::randomSequence(std::string &seq) {
    std::random_device rd;
    std::mt19937 gen(rd());
    if (use_same_gc) {
        std::string GCdistribution = "";
        std::for_each(seq.begin(), seq.end(), [&GCdistribution](char & c) {
            if (c != 'N') {
                GCdistribution += c;
            }
        });
        // OPTION 1 uniform distribution and pick a random char
        std::uniform_int_distribution<> distrib(0, GCdistribution.size() - 1);
        std::for_each(seq.begin(), seq.end(), [&GCdistribution, &distrib, &gen](char & c) {
            if (c != 'N') {
                c = GCdistribution[distrib(gen)];
            }
        });
        // // OPTION 1 shuffle the characters and just put them back in the shuffled order
        // std::random_shuffle(GCdistribution.begin(), GCdistribution.end()); // cannot shuffle N regions..
        // int i = 0;
        // std::for_each(seq.begin(), seq.end(), [&GCdistribution, &i](char & c) {
        //     if (c != 'N') {
        //         c = GCdistribution[i % GCdistribution.size()];
        //         i++;
        //     }
        // });
    } else {
        std::uniform_int_distribution<> distrib(0, validNucleotides.size() - 1);
        std::for_each(seq.begin(), seq.end(), [&distrib, &gen](char & c) {
            if (c != 'N') {
                c = validNucleotides[distrib(gen)];
            }
        });
    }
}

const std::unordered_set<char> Genes::validCharacters ({ 'A', 'C', 'G', 'T', 'N', '-', 'a', 'c' ,'g', 't', 'n' });
const std::unordered_set<char> Genes::replaceIupacCharacters ({ 'R', 'Y', 'S', 'W', 'K', 'M', 'B', 'D', 'H', 'V' });
const std::string Genes::validNucleotides ("ACGT");
const std::vector<std::string> Genes::characterToMask ({
    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", // 0
    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", // 16
    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", // 32
    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", // 48
    "",  // 64
    "A", "CGT", "C", "AGT", "", "", "G", "ACT", "", "", "GT", "", "AC", "ACGT", "", // 65
    "", "", "AG", "GC", "T", "", "ACG", "AT", "", "CT", // 80
    "", "", "", "", "", "", "",  // 90
    "a", "cgt", "c", "agt", "", "", "g", "act", "", "", "gt", "", "ac", "acgt", "", // 97
    "", "", "ag", "gc", "t", "", "acg", "at", "", "ct" // 112
});
void Genes::readFasta(std::string species, std::string fasta) {
    // read contents of fasta and add to the map

    // std::cerr<<" reading species: " << species << " from " << fasta << std::endl;
    std::ifstream f(fasta);
    std::string id, parsedid, seq;
    std::vector<std::string> sequences;
    std::vector<std::string> parsedids;
    if (f.is_open()) {
        while ( getline (f, id) )  {
            if(!id.empty() && id[0] == '>' ) {
                parsedid = id.substr(1, id.find_first_of(':') - 1);
                if(getline(f, seq)) {
                    if(!seq.empty()) {
                        if (fixSequence) fixSequence(seq);
                        if (random) randomSequence(seq);
                        if (shuffle) {
                            sequences.push_back(seq);
                            parsedids.push_back(parsedid);
                        } else {
                            genemap[parsedid] = {species, seq};
                        }
                    } else {
                        std::cerr << "empty seq line..." << std::endl;
                    }
                } else {
                    std::cerr << "incorrect fasta format, no sequence line after id line" << std::endl;
                }
            } else {
                std::cerr << "incorrect fasta format, no id line found" << std::endl;
            }
        }
        f.close();
        if (shuffle) {
            // shuffle ids;
            unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
            std::default_random_engine rng(seed);
            std::shuffle(std::begin(parsedids), std::end(parsedids), rng);
            for (size_t i = 0; i < parsedids.size(); i++) {
                genemap[parsedids[i]] = {species, sequences[i]};
            }
        }
    }


}
