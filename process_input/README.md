## buiild
```bash
# in the folder of process_input:
mkdir build
cd build
cmake ../
make
```

## synopsis
```bash
usage: ./process_input [folder of species subfolders with fasta] [orthology file] [newick file] [outputfolder] [options]
options:
	--rescale_trees: rescales the newick tree if not all species are present in an orthology group.
```

## input requirements
- The folder of species folders with fasta is a folder that contains subfolders that indicate the species name and in each folder a fasta file with gene promotors and corresponding identifiers
- The orthology file contains an orthology group per line like this: ``cluster_id\tnumgenes\tnumspecies\tspace_seperated_genelist``
- The newick file contains a single newick string with all species
