/***************************************************************************
 *   Copyright (C) 2018 Jan Fostier (jan.fostier@ugent.be)                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>
#include <string.h>
#include <unordered_set>
#include "newick.h"
#include "genes.h"
#include "orthology.h"

using namespace std;

int main(int argc, char* argv[])
{
    bool rescale_tree = false;
    bool fix_sequences = true;
    bool shuffle = false;
    bool random = false;
    int cutoff = 3;
    bool use_same_gc = true;
    if(argc < 5) {
        std::cerr << "usage: " << argv[0] << " [folder of species folders with fasta] [orthology file] [newick file] [outputfolder] [options]" << std::endl;
        std::cerr << "options:" << std::endl;
        std::cerr << "\t--rescale_trees: rescales the newick tree if not all species are present in an orthology group." << std::endl;
        std::cerr << "\t--shuffle: Shuffle the orthology group sequences (negative test)." << std::endl;
        std::cerr << "\t--cutoff <int>: Cutoff for minimum number of species in ortho group to be printed. [default 3]." << std::endl;
        std::cerr << "\t--random_seq: Create random sequences of the promotor sequences, with the same GC content as the sequeunce." << std::endl;
        std::cerr << "\t--random_nogc: Create random sequences of the promotor sequences, not taking into account GC content." << std::endl;
        std::cerr << "\t--keep_iupac_characters: keeps iupac characters in the sequence without replacing them with a random representation (will break the sequence to find motifs at that place)." << std::endl;
        return EXIT_FAILURE;
    } else {
        int i = 5;
        bool exit = false;
        while (i < argc && !exit) {
            if(strcmp(argv[i], "--rescale_trees") == 0) {
                rescale_tree = true;
            } else if(strcmp(argv[i], "--keep_iupac_characters") == 0) {
                fix_sequences = false;
            } else if(strcmp(argv[i], "--shuffle") == 0) {
                shuffle = true;
            } else if(strcmp(argv[i], "--random_seq") == 0) {
                random = true;
            } else if(strcmp(argv[i], "--cutoff") == 0) {
                if(i < argc) {
                cutoff = std::stoi(argv[i]);
                } else {
                    std::cerr << "no value given with cutoff" << std::endl;
                    exit = true;
                }
            } else if(strcmp(argv[i], "--random_nogc") == 0) {
                random = true;
                use_same_gc = false;
            } else {
                std::cerr << "Unknown option " << argv[i] << std::endl;
                exit = true;
            }
            i++;
        }
        if(exit) {
            return EXIT_FAILURE;
        }
    }

    std::cerr << "reading fasta files from '" << argv[1] << "'" << std::endl;
    Genes genes(argv[1], fix_sequences, shuffle, random, use_same_gc);
    std::cerr << "reading tree branch lengths from '" << argv[3] << "'" << std::endl;
    Newick newick(argv[3]);
    newick.print_tree(std::cout);
    std::cerr << "reading orthology from '" << argv[2] << "'" << std::endl;
    Orthology orthology(&genes, &newick, argv[2], argv[4], rescale_tree);

    return EXIT_SUCCESS;
}
