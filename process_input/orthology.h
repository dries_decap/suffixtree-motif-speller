#ifndef ORTHOLOGY_H
#define ORTHOLOGY_H


#include <iostream>
#include <fstream>
#include <list>
#include <stack>
#include "genes.h"
#include "newick.h"

class Orthology {
private:
  const char delim = ' ';
  bool rescale_tree = false;
  size_t count = 0;
  int minimimum_species_count = 3;
  void formatGeneList(std::string outputfolder, Genes *genemap, Newick *newick, std::string cluster, std::string genelist);
  void readOrthology(Genes *genemap, Newick *newick, std::string orthologyFile, std::string outputfolder);
public:
  Orthology(Genes *genes, Newick *newick, std::string orthologyFile, std::string outputfolder, bool rescale_tree = false, int minimimum_species_count = 3) : rescale_tree(rescale_tree), count(0), minimimum_species_count(minimimum_species_count) {
    readOrthology(genes, newick, orthologyFile, outputfolder);
  }
};


#endif
