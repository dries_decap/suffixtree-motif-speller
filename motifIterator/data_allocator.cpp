
#include <stdio.h>
#include <stdlib.h>
#include "data_allocator.h"

// ARENA
memory_pool_arena::memory_pool_arena(size_t arena_size, size_t item_size) : item_size(item_size), storage(new char[arena_size*item_size]) {
    // std::cerr << "new memory pool created with item size " << item_size << std::endl;
    memset(storage, 0, arena_size*item_size);
    for (size_t i = 1; i < arena_size; i++) {
        // std::cerr << "set address at first address to : " << (void *)(&storage[i*item_size]) << std::endl;
        ((char **)(storage + (i - 1)*item_size))[0] = (char *)(storage + i*item_size);
        // std::cerr << "set address at first address to : " << (void *)(&storage[(i - 1)*item_size)]) << std::endl;
    }
    // ((void *)(storage[(arena_size - 1)*item_size]))[0] = NULL; // should already be 0 from memset
}
memory_pool_arena::~memory_pool_arena() {
    free_arena();
}
void memory_pool_arena::free_arena() {
    delete[] storage;
}
// Sets the next arena. Used when the current arena is full and
// we have created this one to get more storage.
void memory_pool_arena::set_next_arena(memory_pool_arena *new_arena) {
    assert(!next);
    next = new_arena;
}
char *memory_pool_arena::get_storage() {
    return storage;
}
memory_pool_arena *memory_pool_arena::get_next_arena() {
    return next;
}


// MEMORY_POOL
node_type memory_pool::get_node_type(const bool withBlsVector) {
    if(withBlsVector) return FULL_NODE_WITH_BLSVECTOR;
    else return FULL_NODE_WITHOUT_BLSVECTOR;
}
node_type memory_pool::get_node_type(const bool withBlsVector, const bool withChildren) {
    if(withBlsVector && !withChildren) return SPARSE_NODE_WITH_BLSVECTOR_WITHOUT_CHILD; // most common, last level
    else if(!withBlsVector && withChildren) return SPARSE_NODE_WITHOUT_BLSVECTOR_WITH_CHILD;
    else if(withBlsVector && withChildren) return SPARSE_NODE_WITH_BLSVECTOR_WITH_CHILD;
    else return SPARSE_NODE_WITHOUT_BLSVECTOR_WITHOUT_CHILD; // will probably never be used!
}

std::string memory_pool::node_type_descriptions[NODE_TYPE_COUNT] = {
    "Full node",
    "Full node & blsvector",
    "Sparse node",
    "Sparse node & children",
    "Sparse node & blsvector",
    "Sparse node & blsvector & children",
};

double memory_pool::get_capacity_in_GB() {
    return (double)storage_capacity / 1000 / 1000 / 1000;
}
double memory_pool::get_capacity_in_GiB() {
    return (double)storage_capacity / 1024 / 1024 / 1024;
}
void memory_pool::detailed_memory_analysis() {
    size_t mem = 0;
    size_t i = 0;
    for (size_t t = 0; t < NODE_TYPE_COUNT; t++) {
        mem = item_count[t]*item_sizes[t];
        i = 0;
        while(i < 6 && mem > 1024) {
            mem = mem / 1024;
            i++;
        }
        std::cerr << "[" << node_type_descriptions[t] << "] " << item_count[t] << " elements of size " << item_sizes[t] <<  " == " <<  mem << memory_prefix[i] << "B"  << std::endl;
    }
    std::cerr << "[TOTAL pool storage] ";
    mem = current_storage;
    i = 0;
    while(i < 6 && mem > 1024) {
        mem = mem / 1024;
        i++;
    }
    std::cerr << mem << memory_prefix[i] << "B / ";
    mem = storage_capacity;
    i = 0;
    while(i < 6 && mem > 1024) {
        mem = mem / 1024;
        i++;
    }
    std::cerr << mem << memory_prefix[i] << "B" << (storage_capacity > 0 ? " [" + std::to_string(100*current_storage / storage_capacity) + "%]" : "") << std::endl;

}

void memory_pool::memory_analysis() {
    std::cerr << "pool storage capacity is at ";
    size_t mem = current_storage;
    size_t i = 0;
    while(i < 6 && mem > 1024) {
        mem = mem / 1024;
        i++;
    }
    std::cerr << mem << memory_prefix[i] << "B / ";
    mem = storage_capacity;
    i = 0;
    while(i < 6 && mem > 1024) {
        mem = mem / 1024;
        i++;
    }
    std::cerr << mem << memory_prefix[i] << "B" << (storage_capacity > 0 ? " [" + std::to_string(100*current_storage / storage_capacity) + "%]" : "") << std::endl;
}

char *memory_pool::alloc(const node_type t) {
    char *current_item = getNextFreeItem(t);

    updateStorage(item_sizes[t]);
    updateElementCount(1, t);
    return current_item;
}
char *memory_pool::alloc(const bool withBlsVector) {
    return alloc(get_node_type(withBlsVector));
}
char *memory_pool::alloc(const bool withBlsVector, const bool withChildren) {
    return alloc(get_node_type(withBlsVector, withChildren));
}

void memory_pool::free(char *data, const node_type t) {
    // memset(data, 0, item_sizes[t]); // reset data of node // not needed as it is done in the concurrentmotifmap
    updateFreeList(data, t);

    updateStorage(-item_sizes[t]);
    updateElementCount(-1, t);
}
void memory_pool::free(char *node, const bool withBlsVector) {
    free(node, get_node_type(withBlsVector));
}
void memory_pool::free(char *node, const bool withBlsVector, const bool withChildren) {
    free(node, get_node_type(withBlsVector, withChildren));
}


memory_pool::memory_pool(const char &blsvectorsize) :
blsvectorsize(blsvectorsize), storage_capacity(0), current_storage(0),
item_sizes{
    1 + IUPAC_FULL_COUNT * sizeof(ConcurrentMotifMapNode),
    1 + IUPAC_FULL_COUNT * sizeof(ConcurrentMotifMapNode) + blsvectorsize * sizeof(blscounttype),
    1 + sizeof(ConcurrentMotifMapNodeSparse),
    1 + sizeof(ConcurrentMotifMapNodeSparse) + sizeof(ConcurrentMotifMapNodeSparse) + IUPAC_FULL_COUNT * sizeof(char),
    1 + sizeof(ConcurrentMotifMapNodeSparse) + blsvectorsize * sizeof(blscounttype),
    1 + sizeof(ConcurrentMotifMapNodeSparse) + sizeof(ConcurrentMotifMapNodeSparse) + IUPAC_FULL_COUNT * sizeof(char) + blsvectorsize * sizeof(blscounttype)
},
items_per_arena{ // round op to nearest 64 fold
    (ARENA_MIN_THRESHOLD / item_sizes[0] / 64 + 1)*64,
    (ARENA_MIN_THRESHOLD / item_sizes[1] / 64 + 1)*64,
    (ARENA_MIN_THRESHOLD / item_sizes[2] / 64 + 1)*64,
    (ARENA_MIN_THRESHOLD / item_sizes[3] / 64 + 1)*64,
    (ARENA_MIN_THRESHOLD / item_sizes[4] / 64 + 1)*64,
    (ARENA_MIN_THRESHOLD / item_sizes[5] / 64 + 1)*64
},
item_count{ 0, 0, 0, 0, 0, 0 }
{
    // std::cerr << "new memory pool created with these sizes: " << std::endl;
    for (size_t i = 0; i < NODE_TYPE_COUNT; i++) {
        // std::cerr << i << ": size is " << item_sizes[i] << " and items per arena is " << items_per_arena[i] << std::endl;
        free_list[i] = NULL;
        arena[i] = NULL;
    }

}
memory_pool::~memory_pool()  {
    // delete all
    detailed_memory_analysis(); // to check if any are left unfreed!
    for (size_t i = 0; i < NODE_TYPE_COUNT; i++) {
        memory_pool_arena *tmp_arena = arena[i];
        memory_pool_arena *cur_arena = tmp_arena;
        while(cur_arena != NULL) {
            storage_capacity -= items_per_arena[i]*item_sizes[i];
            tmp_arena = cur_arena->get_next_arena();
            delete cur_arena;
            cur_arena = tmp_arena;
        }
    }
    // memory_analysis();
}

// concurrent free/arena list update
char *memory_pool::getNextFreeItem(const node_type t) { // TODO these 2 functions!!
    bool done = false;
    char *tmp;
    while (!done) {
        tmp = free_list[t];
        if(tmp != NULL) // can be NULL if some other thread got the last key before us, just skip this loop then
            done = __atomic_compare_exchange_n (&free_list[t], &tmp, ((char **)tmp)[0], false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
        else
            createNewArena(t);
    }
    return tmp;
}

void memory_pool::createNewArena(const node_type t) {
    if(getLockOrValidFreeList(t)) {
        storage_capacity += items_per_arena[t]*item_sizes[t];
        memory_pool_arena *new_arena = new memory_pool_arena(items_per_arena[t], item_sizes[t]);
        new_arena->set_next_arena(arena[t]);
        arena[t] = new_arena;
        free_list[t] = new_arena->get_storage();
        freeLock(t);
    }
}
void memory_pool::updateFreeList(const char *new_free_item, const node_type t) {
    bool done = false;
    while (!done) {
        ((char **)new_free_item)[0] = free_list[t];
        done = __atomic_compare_exchange_n (&free_list[t], &new_free_item, ((char **)new_free_item)[0], false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
    }
}
// concurrent meta data update
void memory_pool::updateCapacity(const int add) {
    bool done = false;
    size_t tmp;
    while (!done) {
        tmp = storage_capacity;
        done = __atomic_compare_exchange_n (&storage_capacity, &tmp, tmp + add, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
    }
}
void memory_pool::updateStorage(const int add) {
    bool done = false;
    size_t tmp;
    while (!done) {
        tmp = current_storage;
        done = __atomic_compare_exchange_n (&current_storage, &tmp, tmp + add, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
    }
}
void memory_pool::updateElementCount(const int add, const node_type t) {
    bool done = false;
    size_t tmp;
    while (!done) {
        tmp = item_count[t];
        done = __atomic_compare_exchange_n (&item_count[t], &tmp, tmp + add, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
    }
}


//  locks per type
bool memory_pool::getLockOrValidFreeList(const node_type t) {
    // std::cerr << "getting for lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
    bool gotLock = false;
    while(free_list[t] == NULL && !gotLock) {
        gotLock = tryLock(t);
        // std::cerr << "waiting for lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
        std::this_thread::sleep_for(std::chrono::nanoseconds(1)); // allow interruption of thread
    }
    return gotLock;
    // std::cerr << "got lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
}

void memory_pool::blockingGetLock(const node_type t) {
    // std::cerr << "getting for lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
    while(!tryLock(t)) {
        // std::cerr << "waiting for lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
        std::this_thread::sleep_for(std::chrono::nanoseconds(1)); // allow interruption of thread
    }
    // std::cerr << "got lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
}
bool memory_pool::tryLock(const node_type t) {
    char flag = 1 << t;
    if(concurrency_bit & flag) {
        // lock not free
        // std::cerr << "flag: " << +flag << std::endl;
        return false;
    }
    // std::cerr << "locking: " << +flag << std::endl;
    bool updatelock = false;
    char val;
    while ((concurrency_bit & flag) == 0 && !updatelock) {
        val = concurrency_bit;
        updatelock = __atomic_compare_exchange_n (&concurrency_bit, &val, val | flag, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
    }
    return updatelock;
}
void memory_pool::freeLock(const node_type t) {
    // std::cerr << "freeing lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
    // assert((data & FLAG_UPDATE) > 0);
    char flag = 1 << t;
    bool freelock = false;
    char val;
    while ((concurrency_bit & flag) > 0 && !freelock) {
        val = concurrency_bit;
        freelock = __atomic_compare_exchange_n (&concurrency_bit, &val, val & ~flag, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
    }
    // std::cerr << "lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
}
