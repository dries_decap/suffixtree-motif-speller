## build

```bash
# apache arrow/parquet is required and can be installed as described here https://arrow.apache.org/install/
# similarly google/marl is used for multithreading and is required
# UBUNTU/DEBIAN summary (for other follow the link above)
sudo apt-get update
sudo apt-get install -y -V ca-certificates lsb-release wget
wget https://apache.jfrog.io/artifactory/arrow/$(lsb_release --id --short | tr 'A-Z' 'a-z')/apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
sudo apt-get install -y -V ./apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
sudo apt-get update
sudo apt-get install -y -V libarrow-dev libarrow-glib-dev libparquet-dev libparquet-glib-dev
## uuid/uuid.h is required, on ubuntu:
sudo apt-get install -y uuid-dev

# in the folder of motifIterator:
git clone https://github.com/google/marl.git
mkdir build
cd build
cmake ../
make
```

## synopsis
```bash
usage: motifiterator mode input [options]
  mode:  Either discovery or match.
  input: Input file or '-' for stdin.
  options:
    [required]:
        --bls (string): provide the BLS threshold (between 0 and 1) list. Example: '0.15,0.5,0.6,0.7,0.9,0.95'. [Required]
        --lengthrange (string): give the length range of searched motifs. Example: '5,8', this gives lengths 5,6 and 7. [length option required]
        --length (int): give the length of searched motifs [length option required]
    [optional]:
        --count:        count the number of occurences of each motif in different orthologous gene groups.
        --AF:   alignment free motif discovery [default]
        --AB:   alignment based motif discovery
        --skipRC:       do not use the reverse complement of the promotor region to discover/match motifs.
        --printRep:     only used with '--skipRC', prints the representative of the motif rather than the actual found motif to group with the RC of the motif.
        --exact:        use the alphabet with exact nucleotides only (A,C,G,T) [default]
        --exactAndN:    use the alphabet with exact and N nucleotides only (A,C,G,T,N)
        --exactTwofoldAndN:     use the alphabet with exact, twofold and N (A,C,G,T,R,Y,S,W,K,M,N)
        --fullIupac:    use the full iupac alphabet (A,C,G,T,R,Y,S,W,K,M,B,D,H,V,N)
        --degen (int):  give the number of degenerate characters of searched motifs
        --minbls (float):       give the min bls threshold to emit motifs in match mode
        --parquet:      parquet output [only available in discovery mode]
        --ascii:        string output [only available in discovery mode]
        --output (string):      output file to write output to [default is stdout]

```


## input requirements
- the input is a file (or stdin stream) with one or more orthology clusters produces by the ``process_input`` tool
