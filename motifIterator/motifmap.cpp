#include <thread>
#include "motifmap.h"
#include "malloc.h"

// full node
MotifMapNodeFull::MotifMapNodeFull(const char &blsvectorsize) {
    init(blsvectorsize);
}
void MotifMapNodeFull::init(const char &blsvectorsize) {
    size_t datasize = 1 + IUPAC_FULL_COUNT * sizeof(MotifMapNode) + (blsvectorsize > 0 ? blsvectorsize * sizeof(blscounttype) : 0);
    data = (char *)malloc(datasize);
    memset(data, 0, datasize);
    data[0] = data[0] | ((blsvectorsize > 0 ? FLAG_HAS_BLS_VECTOR : 0));
}
blscounttype *MotifMapNodeFull::getBlsVector() {
    return (blscounttype *)&data[1 + IUPAC_FULL_COUNT * sizeof(MotifMapNode)];
}
MotifMapNode *MotifMapNodeFull::getChildren(const char &blsvectorsize) {
    return (MotifMapNode *)&data[1];
}
void MotifMapNodeFull::addBlsVector(const char &val) {
    if(data[0] & FLAG_HAS_BLS_VECTOR) {
        blscounttype *blsvec = getBlsVector();
        for (int i = 0; i < val; i++) {
            blsvec[i]++;
        }
    } else {
        std::cerr << "trying to add a blsvector in a node that doesn't have a blsvector!" << std::endl;
    }
}
void MotifMapNodeFull::addMotifToMap(const std::string &motif, const char &pos, const char &val, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize) {
    if(pos == motif.length()) {
        addBlsVector(val);
    } else {
        MotifMapNode *children = getChildren(blsvectorsize);
        int childidx = IupacMask::characterToMask[motif[pos]].getMask() - 1;
        if(pos + 1 < numdegen) {
            ((MotifMapNodeFull *)children)[childidx].addMotifToMap(motif, pos + 1, val, numdegen, range, blsvectorsize);
        } else if (pos < range.second - 3){ // from: pos + 1 < range.second - 2
            ((MotifMapNodeSparse *)children)[childidx].addMotifToMap(motif, pos + 1, val, numdegen, range, blsvectorsize);
        } else {
            ((MotifMapNodeLeaves *)children)[childidx].addMotifToMap(motif, pos + 1, val, numdegen, range, blsvectorsize);
        }
    }
}
void MotifMapNodeFull::recPrintAndDelete(const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout) {
    if(data[0] & FLAG_HAS_BLS_VECTOR) {
        blscounttype *blsvec = getBlsVector();
        printMotif(currentmotif, blsvec, unique_count, range, blsvectorsize, parquetout);
    }
    MotifMapNode *children = getChildren(blsvectorsize);
    if(currentmotif.length() < numdegen) {
        for(int i = 0; i < IUPAC_FULL_COUNT; i++) {
            ((MotifMapNodeFull *)children)[i].recPrintAndDelete(currentmotif + IupacMask::representation[i + 1], unique_count, numdegen, range, blsvectorsize, parquetout);
        }
    } else if (currentmotif.length() < range.second - 3){
        for(int i = 0; i < IUPAC_FULL_COUNT; i++) {
            if(data[i + 1] > -1) {
                ((MotifMapNodeSparse *)children)[data[i + 1]].recPrintAndDelete(currentmotif + IupacMask::representation[i + 1], unique_count, numdegen, range, blsvectorsize, parquetout);
            }
        }
    } else {
        for (int i = 0; i < IUPAC_FULL_COUNT; i++) {
            if(data[i + 1] > -1) {
                ((MotifMapNodeLeaves *)children)[data[i + 1]].recPrintAndDelete(currentmotif + IupacMask::representation[i + 1], unique_count, numdegen, range, blsvectorsize, parquetout);
            }
        }
    }
    free(data);

    // free memory periodically
    if(currentmotif.length() == 2)  {
        malloc_trim(0); // this gives memory back to OS! But don't do this too often
    }
}
void MotifMapNodeFull::recCreateChildren(const std::pair<short, short> &range, const char &blsvectorsize, const char &childrenDepth, const int &maxDepth) {
    // children will be at depth "childrenDepth"
    MotifMapNode *children = getChildren(blsvectorsize);
    if (childrenDepth == maxDepth) {
        // create sparse children
        if(childrenDepth < range.second - 2) { // childrendepth idx is 1 higher than pos idx
            for (int i = 0; i < IUPAC_FULL_COUNT; i++) {
                new ((MotifMapNodeSparse *)children + i) MotifMapNodeSparse(range.first <= childrenDepth ? blsvectorsize : 0);
            }
        } else {
            for (int i = 0; i < IUPAC_FULL_COUNT; i++) {
                new ((MotifMapNodeLeaves*)children + i) MotifMapNodeLeaves(range.first  <= childrenDepth ? blsvectorsize : 0);
            }
        }
    } else {
        for (int i = 0; i < IUPAC_FULL_COUNT; i++) {
            new ((MotifMapNodeFull *)children + i) MotifMapNodeFull(range.first  <= childrenDepth ? blsvectorsize : 0);
            ((MotifMapNodeFull *)children)[i].recCreateChildren(range, blsvectorsize, childrenDepth+1, maxDepth);
        }
    }
}

// sparse node
MotifMapNodeSparse::MotifMapNodeSparse(const char &blsvectorsize) {
    init(blsvectorsize);
}
void MotifMapNodeSparse::init(const char &blsvectorsize) {
    size_t datasize = 1 + 15 * sizeof(char) + (blsvectorsize > 0 ? blsvectorsize * sizeof(blscounttype) : 0);
    data = (char *)malloc(datasize);
    memset(data, 0, datasize);
    data[0] = data[0] | ((blsvectorsize > 0 ? FLAG_HAS_BLS_VECTOR : 0));
    memset(data + 1, -1, IUPAC_FULL_COUNT);
};
blscounttype *MotifMapNodeSparse::getBlsVector() {
    return (blscounttype *)&data[1 + IUPAC_FULL_COUNT * sizeof(char)];
}
MotifMapNode *MotifMapNodeSparse::getChildren(const char &blsvectorsize) {
    return (MotifMapNode *)&data[1 + IUPAC_FULL_COUNT * sizeof(char) + ((data[0] & FLAG_HAS_BLS_VECTOR) ? blsvectorsize * sizeof(blscounttype) : 0)];
}
void MotifMapNodeSparse::addBlsVector(const char &val) {
    if(data[0] & FLAG_HAS_BLS_VECTOR) {
        blscounttype *blsvec = getBlsVector();
        for (int i = 0; i < val; i++) {
            blsvec[i]++;
        }
    } else {
        std::cerr << "trying to add a blsvector in a node that doesn't have a blsvector!" << std::endl;
    }
}

int MotifMapNodeSparse::countChildren() {
    int numChildren = 0;
    for (int i = 0; i < IUPAC_FULL_COUNT; i++) {
        if(data[1+i] >= 0)
            numChildren++;
    }
    return numChildren;
}
void MotifMapNodeSparse::addChild(const int &iupac_idx, const std::pair<short, short> &range, const char &childrenDepth, const char &blsvectorsize) {
    // lock received
    int numChildren = countChildren();
    int datasize =  1 + IUPAC_FULL_COUNT * sizeof(char) + (blsvectorsize > 0 ? blsvectorsize * sizeof(blscounttype) : 0) + (numChildren  + 1)*sizeof(MotifMapNode);
    data = (char *) realloc(data, datasize);
    // init new child
    MotifMapNode *children = getChildren(blsvectorsize);
    if(childrenDepth < range.second - 2) {
        new ((MotifMapNodeSparse *)children + numChildren) MotifMapNodeSparse(range.first <= childrenDepth ? blsvectorsize : 0);
    } else {
        new ((MotifMapNodeLeaves*)children + numChildren ) MotifMapNodeLeaves(range.first  <= childrenDepth ? blsvectorsize : 0);
    }
    data[iupac_idx] = numChildren;
}

void MotifMapNodeSparse::addMotifToMap(const std::string &motif, const char &pos, const char &val, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize) {
    if(pos == motif.length()) {
        addBlsVector(val);
    } else {
        char iupac_idx = IupacMask::characterToMask[motif[pos]].getMask();
        // add child if needed!
        if(data[iupac_idx] == -1) {
            addChild(iupac_idx, range, pos + 1, blsvectorsize);
        }
        MotifMapNode *children = getChildren(blsvectorsize);
        if (pos < range.second - 3){
            ((MotifMapNodeSparse *)children)[data[iupac_idx]].addMotifToMap(motif, pos + 1, val, numdegen, range, blsvectorsize);
        } else {
            ((MotifMapNodeLeaves *)children)[data[iupac_idx]].addMotifToMap(motif, pos + 1, val, numdegen, range, blsvectorsize);
        }
    }
}

void MotifMapNodeSparse::recPrintAndDelete(const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout) {
    if(data[0] & FLAG_HAS_BLS_VECTOR) {
        blscounttype *blsvec = getBlsVector();
        printMotif(currentmotif, blsvec, unique_count, range, blsvectorsize, parquetout);
    }
    MotifMapNode *children = getChildren(blsvectorsize);
    if (currentmotif.length() < range.second - 3){
        for(int i = 0; i < IUPAC_FULL_COUNT; i++) {
            if(data[i + 1] > -1) {
                ((MotifMapNodeSparse *)children)[data[i + 1]].recPrintAndDelete(currentmotif + IupacMask::representation[i + 1], unique_count, numdegen, range, blsvectorsize, parquetout);
            }
        }
    } else {
        for (int i = 0; i < IUPAC_FULL_COUNT; i++) {
            if(data[i + 1] > -1) {
                ((MotifMapNodeLeaves *)children)[data[i + 1]].recPrintAndDelete(currentmotif + IupacMask::representation[i + 1], unique_count, numdegen, range, blsvectorsize, parquetout);
            }
        }
    }
    free(data);

    // free memory periodically
    if(currentmotif.length() == 2)  {
        malloc_trim(0); // this gives memory back to OS! But don't do this too often
    }
}

// leaves
int MotifMapNodeLeaves::countChildren() {
    int numChildren = 0;
    for (int i = 0; i < IUPAC_FULL_COUNT; i++) {
        if(data[1+i] >= 0)
            numChildren++;
    }
    return numChildren;
}
blscounttype *MotifMapNodeLeaves::getBlsVector() {
    return (blscounttype *)&data[1 + IUPAC_FULL_COUNT * sizeof(char)];
}
blscounttype *MotifMapNodeLeaves::getBlsVector(const int &childidx, const char &blsvectorsize) {
    return (blscounttype *)&data[1 + IUPAC_FULL_COUNT * sizeof(char) + ((data[0] & FLAG_HAS_BLS_VECTOR) ? blsvectorsize * sizeof(blscounttype) : 0) + childidx*(blsvectorsize*sizeof(blscounttype))];
}
MotifMapNode *MotifMapNodeLeaves::getChildren(const char &blsvectorsize) {
    return NULL; // not valid as we don't use children here;
}
MotifMapNodeLeaves::MotifMapNodeLeaves(const char &blsvectorsize) {
    init(blsvectorsize);
}
void MotifMapNodeLeaves::init(const char &blsvectorsize) {
    size_t datasize = 1 + 15 * sizeof(char) + (blsvectorsize > 0 ? blsvectorsize * sizeof(blscounttype) : 0);
    data = (char *)malloc(datasize);
    memset(data, 0, datasize);
    data[0] = data[0] | ((blsvectorsize > 0 ? FLAG_HAS_BLS_VECTOR : 0));
    memset(data + 1, -1, IUPAC_FULL_COUNT);
}
void MotifMapNodeLeaves::addBlsVector(const char &val) {
    if(data[0] & FLAG_HAS_BLS_VECTOR) {
        blscounttype *blsvec = getBlsVector();
        for (int i = 0; i < val; i++) {
            blsvec[i]++;
        }
    } else {
        std::cerr << "trying to add a blsvector in a node that doesn't have a blsvector!" << std::endl;
    }
}
void MotifMapNodeLeaves::addBlsVector(const int &childidx, const char &blsvectorsize, const char &val) {
    blscounttype *blsvec = getBlsVector(childidx, blsvectorsize);
    for (int i = 0; i < val; i++)
        blsvec[i]++;
}

void MotifMapNodeLeaves::addChildBlsVector(const int &iupac_idx, const char &blsvectorsize) {
    // lock received
    int numChildren = countChildren();
    int datasize =  1 + IUPAC_FULL_COUNT * sizeof(char) + ((data[0] & FLAG_HAS_BLS_VECTOR) ? blsvectorsize * sizeof(blscounttype) : 0) + (numChildren  + 1)*sizeof(blscounttype)*blsvectorsize;
    data = (char *) realloc(data, datasize);
    // set counts to 0
    blscounttype *blsvec = getBlsVector(numChildren, blsvectorsize);
    memset(blsvec, 0, blsvectorsize * sizeof(blscounttype));
    data[iupac_idx] = numChildren ;
}
void MotifMapNodeLeaves::addMotifToMap(const std::string &motif, const char &pos, const char &val, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize) {
    if(pos == motif.length()) {
        addBlsVector(val);
    } else {
        int iupac_idx = IupacMask::characterToMask[motif[pos]].getMask();
        // create new bls vector if needed!
        if(data[iupac_idx] == -1) {
            addChildBlsVector(iupac_idx, blsvectorsize);
        }

        addBlsVector(data[iupac_idx], blsvectorsize, val);
    }
}
void MotifMapNodeLeaves::recPrintAndDelete(const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout){
    if(data[0] & FLAG_HAS_BLS_VECTOR) {
        blscounttype *blsvec = getBlsVector();
        printMotif(currentmotif, blsvec, unique_count, range, blsvectorsize, parquetout);
    }
    for (int i = 0; i < IUPAC_FULL_COUNT; i++) {
        if(data[i + 1] != -1) {
            blscounttype *blsvec = getBlsVector(data[i + 1], blsvectorsize);
            printMotif(currentmotif + IupacMask::representation[i + 1], blsvec, unique_count, range, blsvectorsize, parquetout);
        }
    }
    free(data);
}

// basic node
void MotifMapNode::printMotif(const std::string &motif, const blscounttype *blsvec, size_t &unique_count, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout) {
    if(blsvec[0] > 0) {
        if(parquetout == NULL) {
            Motif::writeGroupIDAndMotif(motif, std::cout);
            for(int i = 0; i < blsvectorsize; i++) {
                std::cout << "\t" << blsvec[i];
            }
            std::cout << '\n';
        } else {
            *parquetout << Motif::getGroupID(motif).c_str() << motif.c_str();
            for(int i = 0; i < blsvectorsize; i++) {
                *parquetout << (int) blsvec[i];
            }
            *parquetout << parquet::EndRow;
        }
        unique_count++;
 //       if(unique_count % 1000000 == 0) {
 //           std::cerr << "counted " << unique_count << std::endl;
 //       }
    }
}

// map
OriginalMotifMap::OriginalMotifMap(const char &blsvectorsize, const std::pair<short, short> &range, const char &numdegen) : root(NULL), blsvectorsize(blsvectorsize), numdegen(numdegen), range(range) {
    if (numdegen > 0) {
        root = (MotifMapNode *)(new MotifMapNodeFull(0));
        ((MotifMapNodeFull *)root)->recCreateChildren(range, blsvectorsize, 1, numdegen);
    } else {
        root = (MotifMapNode *)(new MotifMapNodeSparse(0));
    }
}
void OriginalMotifMap::deleteRoot() {
    size_t unique_count;
    root->recPrintAndDelete("", unique_count, numdegen, range, blsvectorsize, NULL);
    delete root;
}
void OriginalMotifMap::addMotifToMap(const std::string &motif, const char &val) {
    root->addMotifToMap(motif, 0, val, numdegen, range, blsvectorsize);
}
void OriginalMotifMap::recPrintAndDelete(size_t &unique_count, parquet::StreamWriter *parquetout) {
    root->recPrintAndDelete("", unique_count, numdegen, range, blsvectorsize, parquetout);
    delete root;
}
