#ifndef GENEFAMILY_H
#define GENEFAMILY_H


#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>
#include <thread>
#include "suffixtree.h"
#include "malloc.h"
#include "uuid/uuid.h"

#define UUID_STRING_LENGTH 37
enum BLSSpellerMode { DISCOVERY = 0x0, MATCH = 0x1 };
enum ALignmentType { ALIGNMENT_BASED = 0x0, ALIGNMENT_FREE = 0x1 };

class GeneFamily {
protected:
    static const std::string getPathFromURI(const std::string uri) ;
    static const std::unordered_set<char> validCharacters;
    char my_uuid[UUID_STRING_LENGTH]={0};
    std::chrono::time_point<std::chrono::system_clock> startChrono();
    double stopChrono(std::chrono::time_point<std::chrono::system_clock> prevTime);
    size_t getIndexOfVector(const std::vector<std::string> &v, const std::string &val);
    void get_uuid();

public:
    GeneFamily() { get_uuid(); } // std::cerr << "my uuid is " << my_uuid << std::endl;}
    static std::shared_ptr<parquet::schema::GroupNode> GetSchemaCounted(const int &blsThresholdssize);
    virtual void readOrthologousFamily(const BLSSpellerMode mode, std::istream& ifs, const std::string& parquetURI, const std::vector<float> blsThresholds_,
        const Alphabet alphabet, const ALignmentType type, const std::pair<short, short> l, const int maxDegeneration, const bool countBls, const bool useRC,
        const bool printRep, const int threads = 1, const float min_bls = 0.0f, const int max_memory_gb = 0) = 0;

};

class SingleThreadedGeneFamily : public GeneFamily {
public:
    void readOrthologousFamily(const BLSSpellerMode mode, std::istream& ifs, const std::string& parquetURI, const std::vector<float> blsThresholds_,
        const Alphabet alphabet, const ALignmentType type, const std::pair<short, short> l, const int maxDegeneration, const bool countBls, const bool useRC,
        const bool printRep, const int threads = 1, const float min_bls = 0.0f, const int max_memory_gb = 0);

};
class MultiThreadedGeneFamily : public GeneFamily {
public:
    void readOrthologousFamily(const BLSSpellerMode mode, std::istream& ifs, const std::string& parquetURI, const std::vector<float> blsThresholds_,
        const Alphabet alphabet, const ALignmentType type, const std::pair<short, short> l, const int maxDegeneration, const bool countBls, const bool useRC,
        const bool printRep, const int threads = 1, const float min_bls = 0.0f, const int max_memory_gb = 0);

};

#endif
