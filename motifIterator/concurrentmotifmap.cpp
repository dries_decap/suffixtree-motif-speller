#include <thread>
#include "concurrentmotifmap.h"
#include "genefamily.h"
#include "data_allocator.h"
#include "malloc.h"
#include "marl/defer.h"
#include "marl/scheduler.h"
#include "marl/thread.h"


// full node
ConcurrentMotifMapNodeFull::ConcurrentMotifMapNodeFull(const char &blsvectorsize, const bool &hasBlsVector, memory_pool *mp) {
    init(blsvectorsize, hasBlsVector, mp);
}
void ConcurrentMotifMapNodeFull::init(const char &blsvectorsize, const bool &hasBlsVector, memory_pool *mp) {
    size_t datasize = 1 + IUPAC_FULL_COUNT * sizeof(ConcurrentMotifMapNode) + (hasBlsVector ? blsvectorsize * sizeof(blscounttype) : 0);
    // std::cerr << "creating full node" << std::endl;
    // data = (char *)malloc(datasize);
    data = mp->alloc(hasBlsVector);
    memset(data, 0, datasize);
    data[0] = data[0] | FLAG_HAS_CHILDREN | ((hasBlsVector ? FLAG_HAS_BLS_VECTOR : 0)); // always has children!
}
blscounttype *ConcurrentMotifMapNodeFull::getBlsVector() {
    return (blscounttype *)&data[1 + IUPAC_FULL_COUNT * sizeof(ConcurrentMotifMapNode)];
}
ConcurrentMotifMapNode *ConcurrentMotifMapNodeFull::getChildren() {
    return (ConcurrentMotifMapNode *)&data[1];
}
void ConcurrentMotifMapNodeFull::addBlsVector(const char &val) {
    if(data[0] & FLAG_HAS_BLS_VECTOR) {
        blscounttype *blsvec = getBlsVector();
        for (int i = 0; i < val; i++) {
            incrementBlsCount(&blsvec[i]);
        }
    } else {
        std::cerr << "trying to add a blsvector in a node that doesn't have a blsvector!" << std::endl;
    }
}
void ConcurrentMotifMapNodeFull::addMotifToMap(const std::string &motif, const char &pos, const char &val, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, memory_pool *mp) {
    // std::cerr << "fullNode at " << motif[pos] << " at pos " << +pos << std::endl;
    if(pos == motif.length()) { // should not happen
        addBlsVector(val);
    } else {
        ConcurrentMotifMapNode *children = getChildren();
        int childidx = IupacMask::characterToMask[motif[pos]].getMask() - 1;
        if(pos + 1 < numdegen) {
            ((ConcurrentMotifMapNodeFull *)children)[childidx].addMotifToMap(motif, pos + 1, val, numdegen, range, blsvectorsize, mp);
        // } else if (pos < range.second - 3){ // from: pos + 1 < range.second - 2
        } else {
            ((ConcurrentMotifMapNodeSparse *)children)[childidx].addMotifToMap(motif, pos + 1, val, numdegen, range, blsvectorsize, mp);
            // ((ConcurrentMotifMapNodeLeaves *)children)[childidx].addMotifToMap(motif, pos + 1, val, numdegen, range, blsvectorsize);
        }
    }
}
void ConcurrentMotifMapNodeFull::recPrintAndDelete(const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout, const bool &lastLevelOnly, memory_pool *mp) {
    if((data[0] & FLAG_HAS_BLS_VECTOR) && (!lastLevelOnly || (data[0] & FLAG_HAS_CHILDREN) == 0)) {
        blscounttype *blsvec = getBlsVector();
        printMotif(currentmotif, blsvec, unique_count, range, blsvectorsize, parquetout);
    }
    ConcurrentMotifMapNode *children = getChildren();
    if(currentmotif.length() < numdegen) {
        for(int i = 0; i < IUPAC_FULL_COUNT; i++) {
            ((ConcurrentMotifMapNodeFull *)children)[i].recPrintAndDelete(currentmotif + IupacMask::representation[i + 1], unique_count, numdegen, range, blsvectorsize, parquetout, lastLevelOnly, mp);
        }
    } else {
        for(int i = 0; i < IUPAC_FULL_COUNT; i++) {
            ((ConcurrentMotifMapNodeSparse *)children)[i].recPrintAndDelete(currentmotif + IupacMask::representation[i + 1], unique_count, numdegen, range, blsvectorsize, parquetout, lastLevelOnly, mp);
        }
        // delete[] ((ConcurrentMotifMapNodeFull *)children); // TODO somehow this isn't being freed...
    }
}

void ConcurrentMotifMapNodeFull::recPrintAndDeleteInParallel(marl::WaitGroup &wg, const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, const std::string &prefix, const int &level, const bool &lastLevelOnly, const std::shared_ptr<arrow::fs::FileSystem> fs, memory_pool *mp) {
    if (level == 0 || data[0] & FLAG_HAS_BLS_VECTOR) {
        size_t *count_ptr = &unique_count;
        marl::schedule([=] {
            wg.add(1); // add task
            // Before this task returns, decrement the wait group counter.
            // This is used to indicate that the task is done.
            defer(wg.done());

            parquet::StreamWriter *parquet_stream;
            int maxrows = 100*1000; // for parquet
            std::shared_ptr<arrow::io::OutputStream> outfile;
            parquet::WriterProperties::Builder builder;
            std::string output = prefix + currentmotif + ".parquet";
            // std::cerr << "creating thread for " << output << std::endl;

            auto streamresult = fs->OpenOutputStream(output);
            if(!streamresult.ok())
                std::cerr << "ERROR opening output stream: " << streamresult.status() << std::endl;
            outfile = streamresult.ValueOrDie();
            // PARQUET_ASSIGN_OR_THROW(outfile, arrow::io::FileOutputStream::Open(output));
            builder.compression(parquet::Compression::SNAPPY);
            parquet_stream = new parquet::StreamWriter( parquet::ParquetFileWriter::Open(outfile, GeneFamily::GetSchemaCounted(blsvectorsize), builder.build()) );
            parquet_stream->SetMaxRowGroupSize(maxrows);
            size_t local_unique_counts = 0;
            this->recPrintAndDelete(currentmotif, local_unique_counts, numdegen, range, blsvectorsize, parquet_stream, lastLevelOnly, mp);

            // cleanup
            parquet_stream->EndRowGroup();
            delete parquet_stream;
            outfile->Close();
            if(local_unique_counts == 0) {
                // std::cerr << "empty file: " << output << std::endl;
                auto status = fs->DeleteFile(output);
                if(!status.ok()) {std::cerr << "error deleting empty output file " << output << std::endl;}
            } else { concurrent_add(count_ptr, local_unique_counts); }
        });
    } else {
        ConcurrentMotifMapNode *children = getChildren();
        if(currentmotif.length() + 1 < numdegen) {
            for(int i = 0; i < IUPAC_FULL_COUNT; i++) {
                ((ConcurrentMotifMapNodeFull *)children)[i].recPrintAndDeleteInParallel(wg, currentmotif + IupacMask::representation[i + 1], unique_count, numdegen, range, blsvectorsize, prefix, level - 1, lastLevelOnly, fs, mp);
            }
        } else {
            for(int i = 0; i < IUPAC_FULL_COUNT; i++) {
                ((ConcurrentMotifMapNodeSparse *)children)[i].recPrintAndDeleteInParallel(wg, currentmotif + IupacMask::representation[i + 1], unique_count, numdegen, range, blsvectorsize, prefix, level - 1, lastLevelOnly, fs, mp);
            }
        }
    }
}

void ConcurrentMotifMapNodeFull::recDelete(const char &blsvectorsize, const char &cur_level, const char &level, const char &numdegen, const bool &lastLevelOnly, memory_pool *mp) {
    if(cur_level < level) {
        ConcurrentMotifMapNode *children = getChildren();
        if(cur_level + 1 < numdegen) {
            for(int i = 0; i < IUPAC_FULL_COUNT; i++) {
                ((ConcurrentMotifMapNodeFull *)children)[i].recDelete(blsvectorsize, cur_level + 1, level, numdegen, lastLevelOnly, mp);
            }
        } else {
            for(int i = 0; i < IUPAC_FULL_COUNT; i++) {
                ((ConcurrentMotifMapNodeSparse *)children)[i].recDelete(blsvectorsize, cur_level + 1, level, numdegen, lastLevelOnly, mp);
            }
            // delete[] ((ConcurrentMotifMapNodeFull *)children); // TODO somehow this isn't being freed...
        }
    }
    if (lastLevelOnly && cur_level == level - 1){
        std::cerr << "rec delete" << std::endl;
        mp->free(data, data[0] & FLAG_HAS_BLS_VECTOR);
        data = NULL;
    }
}


void ConcurrentMotifMapNodeFull::recCreateChildren(const std::pair<short, short> &range, const char &blsvectorsize, const char &childrenDepth, const int &maxDepth, memory_pool *mp) {
    // children will be at depth "childrenDepth"
    ConcurrentMotifMapNode *children = getChildren();
    if (childrenDepth == maxDepth) {
        for (int i = 0; i < IUPAC_FULL_COUNT; i++) {
            new ((ConcurrentMotifMapNodeSparse *)children + i) ConcurrentMotifMapNodeSparse(blsvectorsize, range.first <= childrenDepth, childrenDepth < range.second - 1, mp);
        }
    } else {
        for (int i = 0; i < IUPAC_FULL_COUNT; i++) {
            new ((ConcurrentMotifMapNodeFull *)children + i) ConcurrentMotifMapNodeFull(blsvectorsize, range.first  <= childrenDepth, mp);
            ((ConcurrentMotifMapNodeFull *)children)[i].recCreateChildren(range, blsvectorsize, childrenDepth+1, maxDepth, mp);
        }
    }
}

// sparse node
ConcurrentMotifMapNodeSparse::ConcurrentMotifMapNodeSparse(const char &blsvectorsize, const bool &hasBlsVector, const bool &hasChildren, memory_pool *mp) {
    init(blsvectorsize, hasBlsVector, hasChildren, mp);
}
void ConcurrentMotifMapNodeSparse::init(const char &blsvectorsize, const bool &hasBlsVector, const bool &hasChildren, memory_pool *mp) {
    size_t datasize = 1 + // flag
                      sizeof(ConcurrentMotifMapNodeSparse) + // sibling
                      (hasChildren ? sizeof(ConcurrentMotifMapNodeSparse) + IUPAC_FULL_COUNT * sizeof(char) : 0) + // child + child indexes
                      (hasBlsVector ? (blsvectorsize * sizeof(blscounttype))  : 0); // bls vector
    // std::cerr << "creating sparse node" << std::endl;
    // data = (char *)malloc(datasize);
    data = mp->alloc(hasBlsVector, hasChildren);
    memset(data, 0, datasize);
    data[0] = data[0] | (hasBlsVector ? FLAG_HAS_BLS_VECTOR : 0) | (hasChildren ? FLAG_HAS_CHILDREN : 0);
    if(hasChildren) {
        memset(data + 1 + sizeof(ConcurrentMotifMapNodeSparse) * 2, -1, IUPAC_FULL_COUNT); // set children to -1
    }
};
blscounttype *ConcurrentMotifMapNodeSparse::getBlsVector() {
    return (blscounttype *)&data[1 + sizeof(ConcurrentMotifMapNodeSparse) + ((data[0] & FLAG_HAS_CHILDREN) > 0 ? sizeof(ConcurrentMotifMapNodeSparse) + IUPAC_FULL_COUNT * sizeof(char) : 0)];
}

ConcurrentMotifMapNodeSparse *ConcurrentMotifMapNodeSparse::getFirstSibling() {
    return (ConcurrentMotifMapNodeSparse *)&data[1];
}
ConcurrentMotifMapNodeSparse *ConcurrentMotifMapNodeSparse::getFirstChild() {
    return (ConcurrentMotifMapNodeSparse *)&data[1 + sizeof(ConcurrentMotifMapNodeSparse)];
}
ConcurrentMotifMapNodeSparse *ConcurrentMotifMapNodeSparse::getLastSibling() {
    ConcurrentMotifMapNodeSparse *sibling = getFirstSibling();
    if(sibling->isInitialised()) {
        while(sibling->isInitialised()) {
            sibling = sibling->getFirstSibling();
        }
    }
    return sibling;
}
ConcurrentMotifMapNodeSparse *ConcurrentMotifMapNodeSparse::getOrCreateChildForIupacChar(const char &blsvectorsize, const char &iupac_char, const bool &childHasBlsVector, const bool &childHasChildren, memory_pool *mp) {
    char iupacChildIdxLocation = 1 + sizeof(ConcurrentMotifMapNodeSparse)*2 + IupacMask::characterToMask[iupac_char].getMask() - 1; // iupac - 1, as iupac starts at 1
    if(data[iupacChildIdxLocation] >= 0) {
        // std::cerr << "child (of " <<  (void *)&data[0]<< ") for " << iupac_char << " exists at idx " << +data[iupacChildIdxLocation] << std::endl;
        return getChildAtIndex(data[iupacChildIdxLocation]);
    } else {
        getUpdateLock(data[0]);
        // check if doesnt exit yet, since maybe we waited and another thread created it here..
        if(data[iupacChildIdxLocation] >= 0) {
            // std::cerr << "child (of " <<  (void *)&data[0]<< ") for " << iupac_char << " created by other thread at idx " << +data[iupacChildIdxLocation] << std::endl;
            freeUpdateLock(data[0]);
            return getChildAtIndex(data[iupacChildIdxLocation]);
        }
        char newindex = 0;
        ConcurrentMotifMapNodeSparse *newchild = createNewChild(blsvectorsize, newindex, childHasBlsVector, childHasChildren, mp);
        data[iupacChildIdxLocation] = newindex;
        // std::cerr << "create new child (of " <<  (void *)&data[0]<< ") for " << iupac_char << " with index " << +newindex << std::endl;
        // assert(newindex < IUPAC_FULL_COUNT);
        freeUpdateLock(data[0]);
        return newchild;
    }
}
ConcurrentMotifMapNodeSparse *ConcurrentMotifMapNodeSparse::createNewChild(const char &blsvectorsize, char &newindex, const bool &childHasBlsVector, const bool &childHasChildren, memory_pool *mp) {
    newindex = 0;
    ConcurrentMotifMapNodeSparse *child = getFirstChild();
    if(child->isInitialised()) {
        newindex++;
        while(child->getFirstSibling()->isInitialised()) {
            child = child->getFirstSibling();
            newindex++;
        }
        return child->createNewSibling(blsvectorsize, mp);
    } else {
        return createNewFirstChild(blsvectorsize, childHasBlsVector, childHasChildren, mp);
    }
}
ConcurrentMotifMapNodeSparse *ConcurrentMotifMapNodeSparse::createNewSibling(const char &blsvectorsize, memory_pool *mp) {
    ConcurrentMotifMapNodeSparse *ptr = getFirstSibling();
    new (ptr) ConcurrentMotifMapNodeSparse(blsvectorsize, data[0] & FLAG_HAS_BLS_VECTOR, data[0] & FLAG_HAS_CHILDREN, mp);
    return ptr;
}
ConcurrentMotifMapNodeSparse *ConcurrentMotifMapNodeSparse::createNewFirstChild(const char &blsvectorsize, const bool &childHasBlsVector, const bool &childHasChildren, memory_pool *mp) {
    ConcurrentMotifMapNodeSparse *ptr = getFirstChild();
    new (ptr) ConcurrentMotifMapNodeSparse(blsvectorsize, childHasBlsVector, childHasChildren, mp);
    return ptr;
}

ConcurrentMotifMapNodeSparse *ConcurrentMotifMapNodeSparse::getChildAtIndex(char index) {
    ConcurrentMotifMapNodeSparse  * currnode= getFirstChild();
    if(index == 0) return currnode;
    // std::cerr << "got first child and need sibling " << +index << std::endl;
    while(index > 1) {
        currnode = currnode->getFirstSibling();
        index--;
        // std::cerr << +index << " siblings left" << std::endl;
    }
    // std::cerr << "At index " << +index << ", return sibling" << std::endl;
    return currnode->getFirstSibling();
}
ConcurrentMotifMapNodeSparse *ConcurrentMotifMapNodeSparse::getSiblingAtIndex(char index) {
    ConcurrentMotifMapNodeSparse  *currnode = this;
    if(index == 0) return currnode;
    while(index > 1) {
        currnode = currnode->getFirstSibling();
        index--;
    }
    return currnode->getFirstSibling();
}
void ConcurrentMotifMapNodeSparse::addBlsVector(const char &val) {
    if(data[0] & FLAG_HAS_BLS_VECTOR) {
        blscounttype *blsvec = getBlsVector();
        for (int i = 0; i < val; i++) {
            incrementBlsCount(&blsvec[i]);
        }
    } else {
        std::cerr << "trying to add a blsvector in a node that doesn't have a blsvector!" << std::endl;
    }
}
void ConcurrentMotifMapNodeSparse::addMotifToMap(const std::string &motif, const char &pos, const char &val, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, memory_pool *mp) {
    if(pos == motif.length()) {
        addBlsVector(val);
    } else {
        // std::cerr << "get child for " << motif << " at pos " << +pos << (pos > range.first - 2 ? " with blsvector" : " without blsvector") << (pos < range.second - 2 ? " and with children" : " and without children") << std::endl;
        ConcurrentMotifMapNodeSparse *child = getOrCreateChildForIupacChar(blsvectorsize, motif[pos], pos > range.first - 2, pos < range.second - 2, mp);
        child->addMotifToMap(motif, pos + 1, val, numdegen, range, blsvectorsize, mp);
    }
}


void ConcurrentMotifMapNodeSparse::deleteRightSiblingsAndSelf(const char &blsvectorsize, memory_pool *mp) {
    // std::cerr << "freeing sparse node" << std::endl;
    if(getFirstSibling()->isInitialised())
        getFirstSibling()->deleteRightSiblingsAndSelf(blsvectorsize, mp);
    // free(data);
    std::cerr << "deleteRightSiblingsAndSelf" << std::endl;
    mp->free(data, data[0] & FLAG_HAS_BLS_VECTOR, data[0] & FLAG_HAS_CHILDREN);
    data = NULL;
}
void ConcurrentMotifMapNodeSparse::recPrintAndDelete(const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout, const bool &lastLevelOnly, memory_pool *mp) {
    if(data[0] & FLAG_HAS_BLS_VECTOR && (!lastLevelOnly || (data[0] & FLAG_HAS_CHILDREN) == 0)) {
        blscounttype *blsvec = getBlsVector();
        printMotif(currentmotif, blsvec, unique_count, range, blsvectorsize, parquetout);
    }
    if(data[0] & FLAG_HAS_CHILDREN) {
        char idxToIupac[IUPAC_FULL_COUNT] = {0};
        for(int i = 0; i < IUPAC_FULL_COUNT; i++) {
            // assert(data[1 + sizeof(ConcurrentMotifMapNodeSparse)*2 + i] < IUPAC_FULL_COUNT);
            if(data[1 + sizeof(ConcurrentMotifMapNodeSparse)*2 + i] > -1) {
                // std::cerr << "found child " << +data[1 + sizeof(ConcurrentMotifMapNodeSparse)*2 + i] << " is " << IupacMask::representation[i + 1] << std::endl;
                idxToIupac[data[1 + sizeof(ConcurrentMotifMapNodeSparse)*2 + i]] = i;
            }
        }
        ConcurrentMotifMapNodeSparse *child = getFirstChild();
        int childidx = 0;
        while(child->isInitialised()) {
            child->recPrintAndDelete(currentmotif + IupacMask::representation[idxToIupac[childidx] + 1], unique_count, numdegen, range, blsvectorsize, parquetout, lastLevelOnly, mp);
            child = child->getFirstSibling();
            childidx++;
        }
        child = getFirstChild();
        if(child->isInitialised() && lastLevelOnly && (child->data[0] & FLAG_HAS_CHILDREN) == 0) { // FREE DATA OF LAST LEVEL NODE IF WE PRINT LAST LEVEL ONLY
            child->deleteRightSiblingsAndSelf(blsvectorsize, mp);
            memset(data + 1 + sizeof(ConcurrentMotifMapNodeSparse), 0, sizeof(ConcurrentMotifMapNodeSparse)); // set children to -1
            memset(data + 1 + sizeof(ConcurrentMotifMapNodeSparse) * 2, -1, IUPAC_FULL_COUNT); // set children to -1
        }
    }
}
void ConcurrentMotifMapNodeSparse::recPrintAndDeleteInParallel(marl::WaitGroup &wg, const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, const std::string &prefix, const int &level, const bool &lastLevelOnly, const std::shared_ptr<arrow::fs::FileSystem> fs, memory_pool *mp) {
    if (level == 0 || data[0] & FLAG_HAS_BLS_VECTOR) {
        size_t *count_ptr = &unique_count;
        marl::schedule([=] {
            wg.add(1); // add task
            // Before this task returns, decrement the wait group counter.
            // This is used to indicate that the task is done.
            defer(wg.done());

            parquet::StreamWriter *parquet_stream;
            int maxrows = 100*1000; // for parquet
            std::shared_ptr<arrow::io::OutputStream> outfile;
            parquet::WriterProperties::Builder builder;
            std::string output = prefix + currentmotif + ".parquet";
            // std::cerr << "creating thread for " << output << std::endl;
            auto streamresult = fs->OpenOutputStream(output);
            if(!streamresult.ok())
                std::cerr << "ERROR opening output stream: " << streamresult.status() << std::endl;
            outfile = streamresult.ValueOrDie();
            // PARQUET_ASSIGN_OR_THROW(outfile, arrow::io::FileOutputStream::Open(output));
            builder.compression(parquet::Compression::SNAPPY);
            parquet_stream = new parquet::StreamWriter( parquet::ParquetFileWriter::Open(outfile, GeneFamily::GetSchemaCounted(blsvectorsize), builder.build()) );
            parquet_stream->SetMaxRowGroupSize(maxrows);
            size_t local_unique_counts = 0;
            this->recPrintAndDelete(currentmotif, local_unique_counts, numdegen, range, blsvectorsize, parquet_stream, lastLevelOnly, mp);

            // cleanup
            parquet_stream->EndRowGroup();
            delete parquet_stream;
            outfile->Close();
            if(local_unique_counts == 0) {
                // std::cerr << "empty file: " << output << std::endl;
                auto status = fs->DeleteFile(output);
                if(!status.ok()) {std::cerr << "error deleting empty output file " << output << std::endl;}
            } else {concurrent_add(count_ptr, local_unique_counts);}
        });
    } else {
        if(data[0] & FLAG_HAS_CHILDREN) {
            char idxToIupac[IUPAC_FULL_COUNT] = {0};
            for(int i = 0; i < IUPAC_FULL_COUNT; i++) {
                if(data[1 + sizeof(ConcurrentMotifMapNodeSparse)*2 + i] > -1) {
                    idxToIupac[data[1 + sizeof(ConcurrentMotifMapNodeSparse)*2 + i]] = i;
                }
            }
            int childidx = 0;
            ConcurrentMotifMapNodeSparse *child = getFirstChild();
            while(child->isInitialised()) {
                child->recPrintAndDeleteInParallel(wg, currentmotif + IupacMask::representation[idxToIupac[childidx] + 1], unique_count, numdegen, range, blsvectorsize, prefix, level - 1, lastLevelOnly, fs, mp);
                child = child->getFirstSibling();
                childidx++;
            }
        }
    }
}
void ConcurrentMotifMapNodeSparse::recDelete(const char &blsvectorsize, const char &cur_level, const char &level, const char &numdegen, const bool &lastLevelOnly, memory_pool *mp) {
    if(cur_level < level) {
        ConcurrentMotifMapNodeSparse *child = getFirstChild();
        while(child->isInitialised()) {
            child->recDelete(blsvectorsize, cur_level + 1, level, numdegen, lastLevelOnly, mp);
            child = child->getFirstSibling();
        }
        child = getFirstChild();
        if(child->isInitialised() && (!lastLevelOnly || (child->data[0] & FLAG_HAS_CHILDREN) == 0)) {
            child->deleteRightSiblingsAndSelf(blsvectorsize, mp);
            memset(data + 1 + sizeof(ConcurrentMotifMapNodeSparse), 0, sizeof(ConcurrentMotifMapNodeSparse)); // set children to -1
            memset(data + 1 + sizeof(ConcurrentMotifMapNodeSparse) * 2, -1, IUPAC_FULL_COUNT); // set children to -1
        }
    }
}


// basic node
void ConcurrentMotifMapNode::printMotif(const std::string &motif, const blscounttype *blsvec, size_t &unique_count, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout) {
    // std::cerr << "printing motif " << motif << std::endl;
    if(blsvec[0] > 0) {
        if(parquetout == NULL) {
            Motif::writeGroupIDAndMotif(motif, std::cout);
            for(int i = 0; i < blsvectorsize; i++) {
                std::cout << "\t" << blsvec[i];
            }
            std::cout << '\n';
        } else {
            *parquetout << Motif::getGroupID(motif).c_str() << motif.c_str();
            for(int i = 0; i < blsvectorsize; i++) {
                *parquetout << (int) blsvec[i];
            }
            *parquetout << parquet::EndRow;
        }
        unique_count++;
 //       if(unique_count % 1000000 == 0) {
 //           std::cerr << "counted " << unique_count << std::endl;
 //       }
    }
}

void ConcurrentMotifMapNode::incrementBlsCount(blscounttype *blscount) {
    bool done = false;
    blscounttype tmp;
    while (!done) {
        tmp = blscount[0];
        done = __atomic_compare_exchange_n (blscount, &tmp, tmp + 1, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
    }
}
void ConcurrentMotifMapNode::concurrent_add(size_t *count, const size_t &localcount) {
    bool done = false;
    size_t tmp;
    while (!done) {
        tmp = count[0];
        done = __atomic_compare_exchange_n (count, &tmp, tmp + localcount, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
    }
}
void ConcurrentMotifMapNode::concurrent_subtract(size_t *count, const size_t &localcount) {
    bool done = false;
    size_t tmp;
    while (!done) {
        tmp = count[0];
        done = __atomic_compare_exchange_n (count, &tmp, tmp - localcount, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
    }
}
void ConcurrentMotifMapNode::getUpdateLock(char &data) {
    // std::cerr << "getting for lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
    while(!checkUpdateLock(data)) {
        // std::cerr << "waiting for lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
        std::this_thread::sleep_for(std::chrono::nanoseconds(1)); // allow interruption of thread
    }
    // std::cerr << "got lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
}
bool ConcurrentMotifMapNode::checkUpdateLock(char &data) {
    if(data & FLAG_UPDATE) {
        // lock not free
        return false;
    }
    bool updatelock = false;
    char val;
    while ((data & FLAG_UPDATE) == 0 && !updatelock) {
        val = data;
        updatelock = __atomic_compare_exchange_n (&data, &val, val | FLAG_UPDATE, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
    }
    return updatelock;
}
void ConcurrentMotifMapNode::freeUpdateLock(char &data) {
    // std::cerr << "freeing lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
    // assert((data & FLAG_UPDATE) > 0);
    bool freelock = false;
    char val;
    while ((data & FLAG_UPDATE) > 0 && !freelock) {
        val = data;
        freelock = __atomic_compare_exchange_n (&data, &val, val & ~FLAG_UPDATE, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED);
    }
    // std::cerr << "lock at address " << (void *)&data << " with data[0] " << +data << std::endl;
}
// map
ConcurrentMotifMap::ConcurrentMotifMap(const char &blsvectorsize, const std::pair<short, short> &range, const char &numdegen, memory_pool *mp) : root(NULL), blsvectorsize(blsvectorsize), numdegen(numdegen), range(range), mp(mp)  {
    if (numdegen > 0) {
        root = (ConcurrentMotifMapNode *)(new ConcurrentMotifMapNodeFull(blsvectorsize, range.first == 1, mp));
        ((ConcurrentMotifMapNodeFull *)root)->recCreateChildren(range, blsvectorsize, 1, numdegen, mp);
    } else {
        root = (ConcurrentMotifMapNode *)(new ConcurrentMotifMapNodeSparse(0, range.first == 1, true, mp));
    }
}
void ConcurrentMotifMap::addMotifToMap(const std::string &motif, const char &val) {
    // std::cerr << "adding motif " << motif << " with bls val " << +val << std::endl;
    root->addMotifToMap(motif, 0, val, numdegen, range, blsvectorsize, mp);
}
void ConcurrentMotifMap::recPrintAndDelete(size_t &unique_count, parquet::StreamWriter *parquetout) {
    root->recPrintAndDelete("", unique_count, numdegen, range, blsvectorsize, parquetout, false, mp);
    delete root;
}
void ConcurrentMotifMap::recPrintAndDeleteInParallel(size_t &unique_count, const std::string &prefix, const int &threads, const bool &lastLevelOnly, const std::shared_ptr<arrow::fs::FileSystem> fs) {
    // mp->detailed_memory_analysis();
    int level = int(log(2*threads)/log(IUPAC_FULL_COUNT)) + 1;
    // std::cerr << "creating threads at level " << level << " of the motitree" << std::endl;
    marl::WaitGroup wg(0);
    root->recPrintAndDeleteInParallel(wg, "", unique_count, numdegen, range, blsvectorsize, prefix, level, lastLevelOnly, fs, mp);
    std::this_thread::sleep_for(std::chrono::milliseconds(1)); // ensures that if 1 task only it is scheduled before we wait
    wg.wait(); // wait for writing all tasks
    // std::cerr << "starting to delete leftover tree" << std::endl;


    // if(!lastLevelOnly) {
    //      // int r = malloc_trim(0);
    //      // std::cerr << "malloc trim returned " << r << std::endl;
    //  // } else {
    //      // root->recDelete(blsvectorsize, 0, level, numdegen, lastLevelOnly, mp);
    //      // delete root;
    //  }
}
