#ifndef CONCURRENTMOTIFMAP_H
#define CONCURRENTMOTIFMAP_H

#include "motif.h"
#include "arrow/io/file.h"
#include "arrow/filesystem/filesystem.h"
#include "marl/waitgroup.h"

class memory_pool;

// concurrent motifmap
// IMPORTANT! BLS vector should be present already if possibly in range, to
// avoid realloc for this.

class ConcurrentMotifMapNode {
protected:
  char *data = NULL;
  void getUpdateLock(char &data);
  bool checkUpdateLock(char &data);
  void freeUpdateLock(char &data);
  void incrementBlsCount(blscounttype *blsvec);
  void concurrent_add(size_t *count, const size_t &localcount);
  void concurrent_subtract(size_t *count, const size_t &localcount);
  void printMotif(const std::string &motif, const blscounttype *blsvec, size_t &unique_count, const std::pair<short, short> &range, const char &blsvectorsize,
                  parquet::StreamWriter *parquetout);
  bool isInitialised() {return data != NULL;}
public:
  virtual void addBlsVector(const char &val) = 0; // assumes a bls vector is present
  virtual void addMotifToMap(const std::string &motif, const char &pos, const char &val, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, memory_pool *mp) = 0;
  virtual void recPrintAndDelete(const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range,
                                 const char &blsvectorsize, parquet::StreamWriter *parquetout, const bool &lastLevelOnly, memory_pool *mp) = 0;
  virtual void recPrintAndDeleteInParallel(marl::WaitGroup &wg, const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range,
                                const char &blsvectorsize, const std::string &prefix, const int &level, const bool &lastLevelOnly, const std::shared_ptr<arrow::fs::FileSystem> fs, memory_pool *mp) = 0;
  virtual void recDelete(const char &blsvectorsize, const char &cur_level, const char &level, const char &numdegen, const bool &lastLevelOnly, memory_pool *mp) = 0;
};

class ConcurrentMotifMapNodeFull : public ConcurrentMotifMapNode {
protected:
  // data structure:
  // data[0] are flags -> flag 0 -> update, flag 1 -> bls vector
  // data[1] == ptr to 15 children
  // if bls vector flag == 1: blsvector = (data[1+15sizeof(pointer)] = bls vector
  ConcurrentMotifMapNode *getChildren();
  blscounttype *getBlsVector();
  void init(const char &blsvectorsize, const bool &hasBlsVector, memory_pool *mp);
public:
  // ~ConcurrentMotifMapNodeFull(memory_pool *mp) { free(data); }
  ConcurrentMotifMapNodeFull(const char &blsvectorsize, const bool &hasBlsVector, memory_pool *mp);
  void addBlsVector(const char &val);
  void addMotifToMap(const std::string &motif, const char &pos, const char &val, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, memory_pool *mp);
  void recPrintAndDelete(const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout, const bool &lastLevelOnly, memory_pool *mp);
  void recCreateChildren(const std::pair<short, short> &range, const char &blsvectorsize, const char &childrenDepth, const int &maxDepth, memory_pool *mp);
  void recPrintAndDeleteInParallel(marl::WaitGroup &wg, const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, const std::string &prefix, const int &level, const bool &lastLevelOnly, const std::shared_ptr<arrow::fs::FileSystem> fs, memory_pool *mp);
  void recDelete(const char &blsvectorsize, const char &cur_level, const char &level, const char &numdegen, const bool &lastLevelOnly, memory_pool *mp);
};

class ConcurrentMotifMapNodeSparse : public ConcurrentMotifMapNode {
  // data structure:
  // first char is flags: always
  // sibling: always
  // child + iupac order if child flag == 1
  // bls vector if bls vector flag == 1
  blscounttype *getBlsVector();
  ConcurrentMotifMapNodeSparse *getFirstChild(); // assumes there are children
  ConcurrentMotifMapNodeSparse *getFirstSibling();
  ConcurrentMotifMapNodeSparse *getLastSibling();
  void deleteRightSiblingsAndSelf(const char &blsvectorsize, memory_pool *mp);
  ConcurrentMotifMapNodeSparse *getSiblingAtIndex(char index);
  ConcurrentMotifMapNodeSparse *getChildAtIndex(char index);
  ConcurrentMotifMapNodeSparse *getOrCreateChildForIupacChar(const char &blsvectorsize, const char &iupac_char, const bool &childHasBlsVector, const bool &childHasChildren, memory_pool *mp);
  ConcurrentMotifMapNodeSparse *createNewChild(const char &blsvectorsize, char &newindex, const bool &childHasBlsVector, const bool &childHasChildren, memory_pool *mp);
  ConcurrentMotifMapNodeSparse *createNewSibling(const char &blsvectorsize, memory_pool *mp);
  ConcurrentMotifMapNodeSparse *createNewFirstChild(const char &blsvectorsize, const bool &childHasBlsVector, const bool &childHasChildren, memory_pool *mp);
  void init(const char &blsvectorsize, const bool &hasBlsVector, const bool &hasChildren, memory_pool *mp);
public:
  // ~ConcurrentMotifMapNodeSparse(memory_pool *mp) { free(data); }
  ConcurrentMotifMapNodeSparse(const char &blsvectorsize, const bool &hasBlsVector, const bool &hasChildren, memory_pool *mp);
  void addBlsVector(const char &val);
  void addMotifToMap(const std::string &motif, const char &pos, const char &val, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, memory_pool *mp);
  void recPrintAndDelete(const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout, const bool &lastLevelOnly, memory_pool *mp);
  void recPrintAndDeleteInParallel(marl::WaitGroup &wg, const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, const std::string &prefix, const int &level, const bool &lastLevelOnly, const std::shared_ptr<arrow::fs::FileSystem> fs, memory_pool *mp);
  void recDelete(const char &blsvectorsize, const char &cur_level, const char &level, const char &numdegen, const bool &lastLevelOnly, memory_pool *mp);
};

class ConcurrentMotifMap : public MotifMap { // second version, less memory usage
private:
  ConcurrentMotifMapNode *root;
  const char blsvectorsize;
  const char numdegen; // is the number of levels with full nodes
  const std::pair<short, short> range;
  memory_pool *mp;

public:
  ConcurrentMotifMap(const char &blsvectorsize, const std::pair<short, short> &range, const char &numdegen, memory_pool *mp);
  ~ConcurrentMotifMap();
  void addMotifToMap(const std::string &motif, const char &val);
  void recPrintAndDelete(size_t &unique_count, parquet::StreamWriter *parquetout);
  void recPrintAndDeleteInParallel(size_t &unique_count, const std::string &prefix, const int &threads, const bool &lastLevelOnly, const std::shared_ptr<arrow::fs::FileSystem> fs);
};
#endif
