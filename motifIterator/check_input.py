#!/bin/env python3

import sys
import os
from Newick_Validator import is_newick


def check_file(file):
    sys.stdout.write("checking file " + file + "\n")
    stream = open(file, "r")
    i = 0
    error = False
    error_msg = ""
    species_seqs = 0
    nr_species = 0
    for line in stream:
        line = line[:-1]
        if i == 0: # name
            sys.stdout.write("checking " + line + "\n")
        elif i == 1: # newick tree
            if not is_newick(line):
                error_msg += "Invalid newick tree; "
                error = True
        elif i == 2:
            if not line.isdigit():
                error_msg += "Invalid number of species; "
                error = True
            else:
                nr_species = int(line)
                sys.stdout.write(line + " species\n")
        else:
            # parse species now:
            # 3(odd) is info, 4(even) is seq
            if i % 2 == 1:
                species_seqs += 1
                tmp = line.split("\t")
                if len(tmp) != 2:
                    error_msg += "gene id list must be split from species name with a tab; "
                    error = True
                else:
                    genes = tmp[0].split(" ")
                    species = tmp[1]
            else:
                seqs = line.split(" ")
                if len(seqs) != len(genes):
                    error_msg += "Number of promotor sequences does not match number of genes provided for species in lines " + str(i) + " and " + str(i + 1) + "; " # line numbers start from 1 instead of 0
                    error = True
                sys.stdout.write(species + "; " + str(len(genes))+ " genes; ")
                lengths = [len(x) - x.count("N") for x in seqs] # remove unused chars from lengths
                sys.stdout.write("lengths [" + str(min(lengths)) + " -> " + str(max(lengths)) + "]\n")
        i += 1
    if species_seqs != nr_species:
        error_msg += "Incorrect number of sequence lines after nr of species line [read " + str(species_seqs) + " species but should be " + str(nr_species) + "]; "
        error = True

    if error:
        sys.stderr.write("incorrect file '" + file + "': " + error_msg + "\n")
        sys.exit(3)
    else:
        sys.stdout.write("correct\n")


if "python" in sys.argv[0]:
    sys.argv.pop(0)

if len(sys.argv) == 2:
    fpath= sys.argv[1]
    if os.path.isdir(fpath):
        if not fpath.endswith("/"):
            fpath = fpath + "/"
        for file in os.listdir(fpath):
            if os.path.isfile(fpath + file):
                check_file(fpath + file)
            else:
                sys.stderr.write("invalid file '" + fpath + file + "' in given path\n")
    elif os.path.isfile(fpath):
        check_file(fpath)
    else:
        sys.stderr.write("invalid file/dir '" + fpath + "'given\n")
else:
    sys.stderr.write("invalid arguments, please provide an input file or input folder\n")
    sys.exit(2)
