#ifndef DATA_ALLOCATOR_H
#define DATA_ALLOCATOR_H

#include <cstdlib>
#include <iostream>
#include <vector>
#include "motif.h"
#include "concurrentmotifmap.h"

// data size greater than this should use mmap (which can be freed in one piece and given back to OS immediately)
#define ARENA_MIN_THRESHOLD 2*1024*1024 // 2 MB per arena // should be more than M_MMAP_THRESHOLD (most likely 128kb according to some documents)

#define NODE_TYPE_COUNT 6
enum node_type {
  FULL_NODE_WITHOUT_BLSVECTOR = 0x0,
  FULL_NODE_WITH_BLSVECTOR = 0x1,
  SPARSE_NODE_WITHOUT_BLSVECTOR_WITHOUT_CHILD = 0x2,
  SPARSE_NODE_WITHOUT_BLSVECTOR_WITH_CHILD = 0x3,
  SPARSE_NODE_WITH_BLSVECTOR_WITHOUT_CHILD = 0x4,
  SPARSE_NODE_WITH_BLSVECTOR_WITH_CHILD = 0x5
};

class memory_pool_arena {
protected:
  const size_t item_size;  // default size per item, will be different for each type: full/sparse node; with/without blsvector; with/without child
  char *storage = NULL; // Storage of this arena.
  memory_pool_arena *next = NULL; // Pointer to the next arena.

public:
  memory_pool_arena(size_t arena_size, size_t item_size) ;
  ~memory_pool_arena();
  void free_arena();
  void set_next_arena(memory_pool_arena *new_arena);
  memory_pool_arena *get_next_arena();
  char *get_storage();
};

class memory_pool {
protected:
    const char blsvectorsize;
    char concurrency_bit = 0;
    size_t storage_capacity;
    size_t current_storage;
    const char memory_prefix[7] = " kMGTP";
    static std::string node_type_descriptions[NODE_TYPE_COUNT];

    const size_t item_sizes[NODE_TYPE_COUNT];
    const size_t items_per_arena[NODE_TYPE_COUNT];
    size_t item_count[NODE_TYPE_COUNT];
    char *free_list[NODE_TYPE_COUNT];
    memory_pool_arena *arena[NODE_TYPE_COUNT];
    node_type get_node_type(const bool withBlsVector);
    node_type get_node_type(const bool withBlsVector, const bool withChildren);

    // concurrency
    bool getLockOrValidFreeList(const node_type t);
    void blockingGetLock(const node_type t);
    bool tryLock(const node_type t);
    void freeLock(const node_type t);

    void updateCapacity(const int add);
    void updateStorage(const int add);
    void updateElementCount(const int add, const node_type t);
    void updateFreeList(const char *new_free_item, const node_type t);
    void createNewArena(const node_type t);
    char *getNextFreeItem(const node_type t);
public:
    memory_pool(const char &blsvectorsize);
    ~memory_pool();
    char *alloc(const node_type t);
    void free(char *node, const node_type t);
    // ease of use functions
    char *alloc(const bool withBlsVector);
    char *alloc(const bool withBlsVector, const bool withChildren);
    void free(char *node, const bool withBlsVector);
    void free(char *node, const bool withBlsVector, const bool withChildren);
    void memory_analysis();
    void detailed_memory_analysis();
    double get_capacity_in_GB();
    double get_capacity_in_GiB();
};

#endif
