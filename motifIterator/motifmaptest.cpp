/***************************************************************************
 *   Copyright (C) 2018 Jan Fostier (jan.fostier@ugent.be)                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <iostream>
#include <fstream>
#include <bitset>
#include "suffixtree.h"
#include "genefamily.h"
#include "concurrentmotifmap.h"

using namespace std;
ConcurrentMotifMap *motifmap;
char blsvectorsize = 8;
std::shared_ptr<parquet::schema::GroupNode> GetSchemaCounted(const char &blsvectorsize) {

    parquet::schema::NodeVector fields;
    fields.push_back(parquet::schema::PrimitiveNode::Make("group", parquet::Repetition::REQUIRED, parquet::Type::BYTE_ARRAY, parquet::ConvertedType::UTF8));
    fields.push_back(parquet::schema::PrimitiveNode::Make("motif", parquet::Repetition::REQUIRED, parquet::Type::BYTE_ARRAY, parquet::ConvertedType::UTF8));
    for(int i = 0; i < blsvectorsize; i++) {
        std::string name = "bls" + std::to_string(i);// std::to_string(blsThresholds[i]);
        fields.push_back(parquet::schema::PrimitiveNode::Make(name.c_str(), parquet::Repetition::REQUIRED, parquet::Type::INT32, parquet::ConvertedType::UINT_16));
    }
    return std::static_pointer_cast<parquet::schema::GroupNode>(parquet::schema::GroupNode::Make("schema", parquet::Repetition::REQUIRED, fields));
}
size_t maxcount = INT_MAX; //  100000;

int main(int argc, char* argv[])
{
    std::pair<short, short> range(6,8);
    int numdegen = 1;

    motifmap = new ConcurrentMotifMap(blsvectorsize, range, numdegen);

    // std::thread t1([](int threadid) {
    //     // open file CLUSTER_10000.motifs.txt
    //     std::ifstream ifs = std::ifstream("CLUSTER_10000.motifs.txt");
    //     std::string line, motif;
    //     int val;
    //     size_t firsttab, secondtab, count = 0;
    //     bool stop = false;
    //     while (!stop && ifs) {
    //         getline(ifs, line);
    //         if(line.length() > 0) {
    //             firsttab = line.find_first_of('\t', 0) + 1;
    //             secondtab = line.find_first_of('\t', firsttab);
    //             motif = line.substr(firsttab , secondtab - firsttab);
    //             val = std::stoi(line.substr(secondtab+1).c_str());
    //             motifmap->addMotifToMap(motif, val);
    //             count++;
    //             if(count >= maxcount) {stop = true;}
    //             if(count % 1000000 == 0)
    //                 std::cerr << "thread " << 1 << " added " << count << " motifs" << std::endl;
    //         }
    //     }
    //     std::cerr << "thread " << 1 << " finished with " << count << " motifs" << std::endl;
    // }, 1);
    // std::thread t2([](int threadid) {
    //     // open file CLUSTER_10000.motifs.txt
    //     std::ifstream ifs = std::ifstream("CLUSTER_10001.motifs.txt");
    //     std::string line, motif;
    //     int val;
    //     size_t firsttab, secondtab, count = 0;
    //     bool stop = false;
    //     while (!stop && ifs) {
    //         getline(ifs, line);
    //         if(line.length() > 0) {
    //             firsttab = line.find_first_of('\t', 0) + 1;
    //             secondtab = line.find_first_of('\t', firsttab);
    //             motif = line.substr(firsttab , secondtab - firsttab);
    //             val = std::stoi(line.substr(secondtab+1).c_str());
    //             motifmap->addMotifToMap(motif, val);
    //             count++;
    //             if(count >= maxcount) {stop = true;}
    //             if(count % 1000000 == 0)
    //                 std::cerr << "thread " << 2 << " added " << count << " motifs" << std::endl;
    //         }
    //     }
    //     std::cerr << "thread " << 2 << " finished with " << count << " motifs" << std::endl;
    // }, 2);
    //
    // // Wait for t1/2 to finish
    // t1.join();
    // t2.join();
    // std::cerr << "all motifs added" << std::endl;


    motifmap->addMotifToMap("AAAAAA", 1);
    motifmap->addMotifToMap("AAAAAAA", 3);
    // motifmap->addMotifToMap("NNTCATCN", 4); // added as wrong motif
    // parquet stuff
    parquet::StreamWriter parquet_stream;
    int maxrows = 100*1000; // for parquet
    std::shared_ptr<arrow::io::FileOutputStream> outfile;
    parquet::WriterProperties::Builder builder;
    PARQUET_ASSIGN_OR_THROW(outfile, arrow::io::FileOutputStream::Open("/tmp/blsspeller_test.parquet"));
    builder.compression(parquet::Compression::SNAPPY);
    parquet_stream = parquet::StreamWriter{ parquet::ParquetFileWriter::Open(outfile, GetSchemaCounted(blsvectorsize), builder.build()) };
    parquet_stream.SetMaxRowGroupSize(maxrows);


    long count = 0;
    motifmap->recPrintAndDelete(count, 2, std::cout, parquet_stream);

    delete motifmap;
    return EXIT_SUCCESS;
}
