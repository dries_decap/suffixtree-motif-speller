#ifndef MOTIFMAP_H
#define MOTIFMAP_H

#include "motif.h"
#include "arrow/io/file.h"

//  motifmap
// IMPORTANT! BLS vector should be present already if possibly in range, to
// avoid realloc for this.

class MotifMapNode {
protected:
  char *data = NULL;
  virtual blscounttype *getBlsVector() = 0;
  virtual MotifMapNode *getChildren(const char &blsvectorsize) = 0;
  void printMotif(const std::string &motif, const blscounttype *blsvec, size_t &unique_count, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout);
  friend class OriginalMotifMap;
public:
  virtual void init(const char &blsvectorsize) = 0;
  virtual void addBlsVector(const char &val) = 0; // assumes a bls vector is present
  virtual void addMotifToMap(const std::string &motif, const char &pos, const char &val, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize) = 0;
  virtual void recPrintAndDelete(const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout) = 0;
  ~MotifMapNode() { };
};

class MotifMapNodeFull : public MotifMapNode {
  // data structure:
  // first char is flags -> flag 0 -> update, flag 1 -> bls vector
  // data[1] == ptr to 15 children
  // if bls vector flag == 1
  // blsvector = (data[1+15sizeof(pointer)] = bls vector
  blscounttype *getBlsVector();
  MotifMapNode *getChildren(const char &blsvectorsize);
public:
  MotifMapNodeFull(const char &blsvectorsize);
  void init(const char &blsvectorsize);
  void addBlsVector(const char &val);
  void addMotifToMap(const std::string &motif, const char &pos, const char &val, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize);
  void recPrintAndDelete(const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout);
  void recCreateChildren(const std::pair<short, short> &range, const char &blsvectorsize, const char &childrenDepth, const int &maxDepth);
};

class MotifMapNodeSparse : public MotifMapNode {
  // data structure:
  // first char is flags -> flag 0 -> update, flag 1 -> bls vector
  // data[1] == ptr to 15 chars to indicate idx of child
  // if bls vector flag == 1
  // blsvector = (data[1+15sizeof(pointer)] = bls vector
  // pointers to children here
  blscounttype *getBlsVector();
  MotifMapNode *getChildren(const char &blsvectorsize);
  int countChildren();
  void addChild(const int &iupac_idx, const std::pair<short, short> &range, const char &childrenDepth, const char &blsvectorsize);
public:
  MotifMapNodeSparse(const char &blsvectorsize);
  void init(const char &blsvectorsize);
  void addBlsVector(const char &val);
  void addMotifToMap(const std::string &motif, const char &pos, const char &val, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize);
  void recPrintAndDelete(const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout);
};

class MotifMapNodeLeaves : public MotifMapNode {
  // data structure:
  // first char is flags -> flag 0 -> update, flag 1 -> bls vector
  // data[1] == ptr to 15 chars to indicate idx of child
  // if bls vector flag == 1
  // blsvector = (data[1+15sizeof(pointer)] = bls vector
  // children bls vectors here
  blscounttype *getBlsVector();
  blscounttype *getBlsVector(const int &childidx, const char &blsvectorsize);
  void addBlsVector(const int &childidx, const char &blsvectorsize, const char &val);
  MotifMapNode *getChildren(const char &blsvectorsize);
  int countChildren();
  void addChildBlsVector(const int &iupac_idx, const char &blsvectorsize);
public:
  MotifMapNodeLeaves(const char &blsvectorsize);
  void init(const char &blsvectorsize);
  void addBlsVector(const char &val);
  void addMotifToMap(const std::string &motif, const char &pos, const char &val, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize);
  void recPrintAndDelete(const std::string currentmotif, size_t &unique_count, const char &numdegen, const std::pair<short, short> &range, const char &blsvectorsize, parquet::StreamWriter *parquetout);
};

class OriginalMotifMap : public MotifMap { // second version, less memory usage
private:
  MotifMapNode *root;
  const char blsvectorsize;
  const char numdegen; // is the number of levels with full nodes
  const std::pair<short, short> range;

public:
  void deleteRoot();
  OriginalMotifMap(const char &blsvectorsize, const std::pair<short, short> &range, const char &numdegen);
  void addMotifToMap(const std::string &motif, const char &val);
  void recPrintAndDelete(size_t &unique_count, parquet::StreamWriter *parquetout);
};
#endif
