
#include <string>
#include <vector>
#include <fstream>
#include <stdexcept>
#include <atomic>
#include "malloc.h"
#include "arrow/filesystem/filesystem.h"
#include "genefamily.h"
#include "data_allocator.h"
#include "marl/defer.h"
#include "marl/scheduler.h"
#include "marl/thread.h"
#include "waitgroupwithcap.h"


// MULTI THREADED GENE FAMILY
void MultiThreadedGeneFamily::readOrthologousFamily(const BLSSpellerMode mode, std::istream& ifs, const std::string& parquetURI, const std::vector<float> blsThresholds_, const Alphabet alphabet,
const ALignmentType type, const std::pair<short, short> l, const int maxDegeneration, const bool countBls, const bool useRC,
const bool printRep, const int threads, const float min_bls, const int max_memory_gb) {

    std::shared_ptr<class arrow::fs::FileSystem> fs;

    std::string parquetOutput = getPathFromURI(parquetURI);
    if(!parquetOutput.empty()) {
        auto fsresult = arrow::fs::FileSystemFromUriOrPath(parquetURI);
        if (!fsresult.ok()) {
          std::cerr << "ERROR getting filesystem: " << fsresult.status();
          throw std::exception();
        }
        fs = fsresult.ValueOrDie();
        auto fileresult = fs->GetFileInfo(parquetOutput);
        if (fileresult.ok()) {
            if (fileresult.ValueOrDie().type() != arrow::fs::FileType::NotFound) {
                if (fileresult.ValueOrDie().type() == arrow::fs::FileType::File) { // should be a directory!
                    std::cerr << "WARNING! output exists as file, will be deleted!" << std::endl;
                    auto status = fs->DeleteFile(parquetOutput);
                    if(!status.ok()) {std::cerr << "error deleting output file " << parquetOutput << std::endl;}
                } else {
                    std::cerr << "WARNING! output already exists, new files will be added with prefix " << my_uuid << std::endl;
                }
            } else {
//                std::cerr << "CREATING output '" << parquetOutput << "' as it does not exist yet. " << fileresult.ValueOrDie().type() << std::endl;
                auto status = fs->CreateDir(parquetOutput);
                if(!status.ok()) {
                    std::cerr << "ERROR creating output directory: " << status << std::endl;
                    throw std::exception();
                }
            }
        } else {
            std::cerr << "ERROR getting file info: " << fileresult.status() << std::endl;
            throw std::exception();
        }
    }

    // only works with mode == 0 and countBls == true
    assert(mode == DISCOVERY && countBls);
    // global variables, like output file
    std::ofstream of;
    parquet::StreamWriter parquet_stream;
    int maxrows = 100*1000; // for parquet
    std::shared_ptr<arrow::io::OutputStream> outfile;
    parquet::WriterProperties::Builder builder;

    int my_threads = threads;
    if (my_threads > marl::Thread::numLogicalCPUs()) {
        my_threads = marl::Thread::numLogicalCPUs();
        std::cerr << "capped number of threads to number of logical CPU cores: " << my_threads << std::endl;
    }

    const std::vector<float> *blsThresholdsPtr = &blsThresholds_;
    const bool *useRCPtr = &useRC;
    const bool *printRepPtr = &printRep;
    const std::pair<short, short> *lPtr = &l;
    const Alphabet *alphabetPtr = &alphabet;
    const int *maxDegenerationPtr = &maxDegeneration;
    const ALignmentType *typePtr = &type;
    int spill = 0;

    marl::Scheduler scheduler(marl::Scheduler::Config().setWorkerThreadCount(my_threads));
    scheduler.bind();
    defer(scheduler.unbind());

    marl::WaitGroupWithCap wg(0, my_threads);
    size_t unique_count = 0;

    std::vector<size_t> counts;
    char blsvectorsize = (unsigned char)blsThresholds_.size(); // assume its less than 256
    memory_pool *mp = new memory_pool(blsvectorsize);
    MotifMap *motifmap = new ConcurrentMotifMap(blsvectorsize, l, maxDegeneration, mp); // if alignment based, save position as first bls value
    while (ifs) {
        // READ DATA
        std::string line;
        getline(ifs, line);
        while(ifs && line.empty()) {getline(ifs, line);}
        if(!ifs || line.empty()) {continue;}
        std::string *name = new std::string(line);
        getline(ifs, line);
        std::string *newick = new std::string(line);
        getline(ifs, line);
        int *N = new int(std::stoi(line));

        std::vector<std::string> *thegenelines = new std::vector<std::string>();
        for (int i = 0; i < *N; i++) {
            getline(ifs, line);
            thegenelines->push_back(line);
            getline(ifs, line);
            thegenelines->push_back(line);
        }

        // check for memory and spill if required
        if(max_memory_gb > 0) {
            // wait for cap
            wg.wait_capacity();
            double mem = mp->get_capacity_in_GB(); //currentMemorySizeGB();
            if(mem > max_memory_gb) {
                std::cerr << "memory exceeds threshold @ " << mem << " GB, requesting data spill..." << std::endl;
                // wait for all threads to block
                wg.wait();
                std::cerr << "spilling motifs to disk, current actual memory before spill is " << mp->get_capacity_in_GB() << "GB" << std::endl;
                ((ConcurrentMotifMap *)motifmap)->recPrintAndDeleteInParallel(unique_count, parquetOutput + my_uuid + "-spill-" + std::to_string(spill) + "-", my_threads, true, fs); // print all levels
                std::cerr << "current actual memory after spill is " << mp->get_capacity_in_GB() << "GB" << std::endl;
                spill += 1;
            }
        }
        marl::schedule([=] {
            wg.add(1); // add task
            // Before this task returns, decrement the wait group counter.
            // This is used to indicate that the task is done.
            defer(wg.done());
            // free created data after task finishes
            defer(delete N);
            defer(delete newick);
            defer(delete name);
            defer(delete thegenelines);

            std::vector<size_t> stringStartPositions;
            std::vector<size_t> next_gene_locations;
            std::vector<std::string> order_of_species;
            std::vector<size_t> order_of_species_mapping;
            std::vector<std::string> gene_names;
            std::string T, line;
            size_t current_pos = 0;
            BLSScore bls(*blsThresholdsPtr, *newick, *N, order_of_species);

            int num_genes = 0;
            stringStartPositions.push_back(0);
            next_gene_locations.push_back(current_pos);
            for (int i = 0; i < (*N)*2; i+=2) {
                line = (*thegenelines)[i];
                // gene names
                std::vector<std::string> genes;
                std::string species = line.substr(line.find_first_of('\t')+1);
                // std::cerr << species << std::endl;
                order_of_species_mapping.push_back(getIndexOfVector(order_of_species, species));
                // std::cerr << (order_of_species_mapping.size() - 1) << " maps to " << order_of_species_mapping[order_of_species_mapping.size() - 1] << std::endl;
                line = line.substr(0, line.find_first_of('\t'));
                size_t start = 0;
                size_t end = line.find_first_of(' ', start);
                while(end != std::string::npos) {
                    genes.push_back(line.substr(start, end - start));
                    start = end + 1;
                    end = line.find_first_of(' ', start);
                }
                genes.push_back(line.substr(start));
                num_genes += genes.size();
                for (size_t k =0; k < genes.size(); k++) {
                    gene_names.push_back(genes[k]);
                }
                if(useRC) { // add RC genes
                    for (size_t k = genes.size() ; k > 0; k--) {
                        gene_names.push_back(genes[k-1]);
                    }
                }
                // genes
                line = (*thegenelines)[i+1];
                if (!T.empty())
                    T.push_back(IupacMask::DELIMITER);
                std::for_each(line.begin(), line.end(), [](char & c) { // replace invalid characters
                    if (validCharacters.find(c) == validCharacters.end())
                        c = IupacMask::INVALID_REPLACEMENT;
                });
                T.append(line);
                stringStartPositions.push_back(T.size() + 1);
                if(useRC) {
                    T.push_back(IupacMask::DELIMITER);
                    T.append(Motif::ReverseComplement(line));
                    stringStartPositions.push_back(T.size() + 1);
                }
                // std::cout << T << std::endl;

                // add gene start locations...
                std::vector<size_t> gene_sizes;
                start = 0;
                end = line.find_first_of(' ', start);
                while(end != std::string::npos) {
                    gene_sizes.push_back(end + 1 - start);
                    start = end + 1;
                    end = line.find_first_of(' ', start);
                }
                gene_sizes.push_back(line.size() + 1 - start);
                for (size_t k =0; k < gene_sizes.size(); k++) {
                    current_pos += gene_sizes[k];
                    next_gene_locations.push_back(current_pos);
                }
                if(useRC) { // add RC genes
                    for (size_t k = gene_sizes.size(); k > 0; k--) {
                        current_pos += gene_sizes[k - 1];
                        next_gene_locations.push_back(current_pos);
                    }
                }
            }
            T.push_back(IupacMask::DELIMITER);
            T.push_back(IupacMask::DELIMITER);

            // std::cerr << "[" << *name << "] " << *N << " gene families and " << num_genes << " genes" << std::endl;
            auto prevTime = startChrono();
            SuffixTree ST(T, *name, *useRCPtr, stringStartPositions, gene_names, next_gene_locations, order_of_species_mapping, motifmap, *printRepPtr);
            int count = ST.printMotifs(*lPtr, *alphabetPtr, *maxDegenerationPtr, bls, *typePtr == ALIGNMENT_BASED, NULL); // 0 == AB, 1 is AF
            size_t iteratorcount = ST.getMotifsIteratedCount();

            double elapsed = stopChrono(prevTime);
            std::cerr << "[" << *name << "] iterated over " << iteratorcount << " motifs" << std::endl;
            std::cerr << "[" << *name << "] counted " << count << " valid motifs in " << elapsed << "s" << std::endl;
            // mp->detailed_memory_analysis();
            // double mem = mp->get_capacity_in_GB();
            // std::cerr << "[" << *name << "] current memory is " << mem << "GB" << std::endl;
        });
        // std::cerr << "enqueued task for " << *name << std::endl;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1)); // ensures that if 1 task only it is scheduled before we wait


    wg.wait(); // wait for all tasks

    auto prevTime = startChrono();

    // std::cerr << "current actual memory is " << currentMemorySizeGB() << "GB" << std::endl;

    if(!parquetOutput.empty()) {
        ((ConcurrentMotifMap *)motifmap)->recPrintAndDeleteInParallel(unique_count, (spill == 0) ? (parquetOutput + my_uuid + "-") : (parquetOutput + my_uuid + "-spill-" + std::to_string(spill) + "-"), my_threads, false, fs); // print all levels
    } else {
        motifmap->recPrintAndDelete( unique_count, NULL);
    }
    double elapsed = stopChrono(prevTime);
    std::cerr << unique_count << " unique motifs counted [in " << elapsed << "s]" << std::endl;
    delete motifmap;
    delete mp;
}
