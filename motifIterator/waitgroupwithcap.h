// Copyright 2019 The Marl Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef marl_waitgroup_withcap_h
#define marl_waitgroup_withcap_h

#include "marl/memory.h"
#include "marl/conditionvariable.h"
#include "marl/debug.h"

#include <limits>
#include <atomic>
#include <mutex>

namespace marl {

class WaitGroupWithCap {
 public:
  // Constructs the WaitGroup with the specified initial count.
  MARL_NO_EXPORT inline WaitGroupWithCap(unsigned int initialCount = 0, unsigned int capacity = std::numeric_limits<unsigned int>::max(), Allocator* allocator = Allocator::Default);

  // add() increments the internal counter by count.
  MARL_NO_EXPORT inline void add(unsigned int count = 1) const;

  // done() decrements the internal counter by one.
  // Returns true if the internal count has reached zero.
  MARL_NO_EXPORT inline bool done() const;

  // wait() blocks until the WaitGroup counter reaches zero.
  MARL_NO_EXPORT inline void wait() const;
  MARL_NO_EXPORT inline void wait_capacity() const;

 private:
  struct Data {
    MARL_NO_EXPORT inline Data(Allocator* allocator);

    std::atomic<unsigned int> count = {0};
    std::atomic<unsigned int> capacity = {std::numeric_limits<unsigned int>::max()};
    ConditionVariable cv;
    marl::mutex mutex;
  };
  const std::shared_ptr<Data> data;
};

WaitGroupWithCap::Data::Data(Allocator* allocator) : cv(allocator) {}

WaitGroupWithCap::WaitGroupWithCap(unsigned int initialCount /* = 0 */,
                                     unsigned int capacity /* = uint max */,
                                     Allocator* allocator /* = Allocator::Default */)
    : data(std::make_shared<Data>(allocator)) {
  data->count = initialCount;
  data->capacity = capacity;
}

void WaitGroupWithCap::add(unsigned int count /* = 1 */) const {
  data->count += count;
}

bool WaitGroupWithCap::done() const {
  MARL_ASSERT(data->count > 0, "marl::WaitGroup::done() called too many times");
  auto count = --data->count;
  {
    marl::lock lock(data->mutex);
    data->cv.notify_all();
  }
  if (count == 0) return true;
  else return false;
}

void WaitGroupWithCap::wait() const {
  marl::lock lock(data->mutex);
  data->cv.wait(lock, [this] { return data->count == 0; });
}

void WaitGroupWithCap::wait_capacity() const {
  marl::lock lock(data->mutex);
  data->cv.wait(lock, [this] { return data->count < data->capacity; });
}
}  // namespace marl

#endif  // marl_waitgroup_withcap_h
