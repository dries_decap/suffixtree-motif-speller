
#include <string>
#include <vector>
#include <fstream>
#include "genefamily.h"


// BASE class functions
std::chrono::time_point<std::chrono::system_clock> GeneFamily::startChrono()
{
        return std::chrono::system_clock::now();
}

void GeneFamily::get_uuid(){
  uuid_t b;
  uuid_generate(b);
  uuid_unparse_lower(b, my_uuid);
}

const std::string GeneFamily::getPathFromURI(const std::string uri) {
    // supported in arrow: file”, “mock”, “hdfs” and “s3fs”
    std::string path;
    if(uri.find("hdfs://", 0) == 0) {// scheme: hdfs://server[:port]/
        path = uri.substr(7);
        auto pos = path.find_first_of('/');
        if(pos == std::string::npos) {std::cerr << "error getting '/' in URI: " << uri << std::endl; return NULL; }
        path = path.substr(pos);
    } else if (uri.find("file://", 0) == 0) { // scheme: file://server[:port]/
        path = uri.substr(7);
        auto pos = path.find_first_of('/');
        if(pos == std::string::npos) {std::cerr << "error getting '/' in URI: " << uri << std::endl; return NULL; }
        path = path.substr(pos);
    } else if (uri.find("mock://", 0) == 0) {// scheme: mock://server[:port]/
        path = uri.substr(7);
        auto pos = path.find_first_of('/');
        if(pos == std::string::npos) {std::cerr << "error getting '/' in URI: " << uri << std::endl; return NULL; }
        path = path.substr(pos);
    } else if (uri.find("s3fs://", 0) == 0) {// scheme: s3fs://server[:port]/
        path = uri.substr(7);
        auto pos = path.find_first_of('/');
        if(pos == std::string::npos) {std::cerr << "error getting '/' in URI: " << uri << std::endl; return NULL; }
        path = path.substr(pos);
    } else {
        path = uri;
    }
    return path;
}

double GeneFamily::stopChrono(std::chrono::time_point<std::chrono::system_clock> prevTime)
{
        std::chrono::duration<double> elapsed = std::chrono::system_clock::now() - prevTime;
        return (elapsed.count());
}
const std::unordered_set<char> GeneFamily::validCharacters ({ 'A', 'C', 'G', 'T', 'N', ' ', '$'  }); // this is used in suffixtree to greatly reduce memory usage by using a fixed array index per character

size_t GeneFamily::getIndexOfVector(const std::vector<std::string> &v, const std::string &val) {
    auto it = find(v.begin(), v.end(), val);

    // If element was found
    int index = -1;
    if (it != v.end())
    {
        index = it - v.begin();
    } else {
        std::cerr << "Species not found in bls tree, will probably crash, please provide the correct names in the tree and the sequences below. " << val << std::endl;
    }
    return index;
}
std::shared_ptr<parquet::schema::GroupNode> GeneFamily::GetSchemaCounted(const int &blsThresholdssize) {

    parquet::schema::NodeVector fields;
    fields.push_back(parquet::schema::PrimitiveNode::Make("group", parquet::Repetition::REQUIRED, parquet::Type::BYTE_ARRAY, parquet::ConvertedType::UTF8));
    fields.push_back(parquet::schema::PrimitiveNode::Make("motif", parquet::Repetition::REQUIRED, parquet::Type::BYTE_ARRAY, parquet::ConvertedType::UTF8));
    for(int i = 0; i < blsThresholdssize; i++) {
        std::string name = "bls" + std::to_string(i);
        fields.push_back(parquet::schema::PrimitiveNode::Make(name.c_str(), parquet::Repetition::REQUIRED, parquet::Type::INT32, parquet::ConvertedType::INT_32));
    }
    return std::static_pointer_cast<parquet::schema::GroupNode>(parquet::schema::GroupNode::Make("schema", parquet::Repetition::REQUIRED, fields));
}

// SINGLE THREADED GENE FAMILY

void SingleThreadedGeneFamily::readOrthologousFamily(const BLSSpellerMode mode, std::istream& ifs, const std::string& parquetURI , const std::vector<float> blsThresholds_, const Alphabet alphabet,
const ALignmentType type, const std::pair<short, short> l, const int maxDegeneration, const bool countBls, const bool useRC,
const bool printRep, const int threads, const float min_bls, const int max_memory_gb) {

    parquet::StreamWriter *parquet_stream;
    int maxrows = 100*1000; // for parquet
    std::shared_ptr<arrow::io::OutputStream> outfile;
    parquet::WriterProperties::Builder builder;
    std::shared_ptr<class arrow::fs::FileSystem> fs;

    std::string parquetOutput = getPathFromURI(parquetURI);
    if(!parquetOutput.empty()) {
        auto fsresult = arrow::fs::FileSystemFromUriOrPath(parquetURI);
        if (!fsresult.ok()) {
          std::cerr << "ERROR getting filesystem: " << fsresult.status();
          throw std::exception();
        }
        fs = fsresult.ValueOrDie();
        auto fileresult = fs->GetFileInfo(parquetOutput);
        if (fileresult.ok()) {
            if (fileresult.ValueOrDie().type() != arrow::fs::FileType::NotFound) {
                if (fileresult.ValueOrDie().type() == arrow::fs::FileType::File) { // should be a directory!
                    std::cerr << "WARNING! output exists as file, will be deleted!" << std::endl;
                    auto status = fs->DeleteFile(parquetOutput);
                    if(!status.ok()) {std::cerr << "error deleting output file " << parquetOutput << std::endl;}
                } else {
                    std::cerr << "WARNING! output already exists, new files will be added with prefix " << my_uuid << std::endl;
                }
            } else {
//                std::cerr << "CREATING output '" << parquetOutput << "' as it does not exist yet. " << fileresult.ValueOrDie().type() << std::endl;
                auto status = fs->CreateDir(parquetOutput);
                if(!status.ok()) {
                    std::cerr << "ERROR creating output directory: " << status << std::endl;
                    throw std::exception();
                }
            }
        } else {
            std::cerr << "ERROR getting file info: " << fileresult.status() << std::endl;
            throw std::exception();
        }
        // parquet things
        auto streamresult = fs->OpenOutputStream((parquetOutput + my_uuid + ".parquet").c_str());
        if(!streamresult.ok())
            std::cerr << "ERROR opening output stream: " << streamresult.status() << std::endl;
        outfile = streamresult.ValueOrDie();
        builder.compression(parquet::Compression::SNAPPY);
        parquet_stream = new parquet::StreamWriter(parquet::ParquetFileWriter::Open(outfile, GetSchemaCounted(blsThresholds_.size()), builder.build()));
        parquet_stream->SetMaxRowGroupSize(maxrows);
    }

    size_t totalCount = 0;
    char blsvectorsize = (unsigned char)blsThresholds_.size(); // assume its less than 256
    OriginalMotifMap motif_to_blsvector_map(blsvectorsize, l, maxDegeneration);
    while (ifs) {
        std::vector<size_t> stringStartPositions;
        std::vector<size_t> next_gene_locations;
        std::vector<std::string> order_of_species;
        std::vector<size_t> order_of_species_mapping;
        std::vector<std::string> gene_names;
        stringStartPositions.push_back(0);
        // READ DATA
        std::string T, newick, line, name;
        int N;
        getline(ifs, line);
        while(ifs && line.empty()) {getline(ifs, line);}
        if(!ifs || line.empty()) {continue;}
        name = line;
        getline(ifs, newick);
        getline(ifs, line);
        N = std::stoi(line);
        BLSScore bls(blsThresholds_, newick, N, order_of_species);
        // std::cerr << bls << std::endl;
        // int nr = 1;
        // for(auto x : order_of_species) {
        //     std::cerr << std::bitset<16>(nr) << "\t" << x << std::endl;
        //     nr = nr << 1;
        // }
        int num_genes = 0;
        size_t current_pos = 0;
        next_gene_locations.push_back(current_pos);
        for (int i = 0; i < N; i++) {
            getline(ifs, line);
            // gene names
            std::vector<std::string> genes;
            std::string species = line.substr(line.find_first_of('\t')+1);
            // std::cerr << species << std::endl;
            order_of_species_mapping.push_back(getIndexOfVector(order_of_species, species));
            // std::cerr << (order_of_species_mapping.size() - 1) << " maps to " << order_of_species_mapping[order_of_species_mapping.size() - 1] << std::endl;
            line = line.substr(0, line.find_first_of('\t'));
            size_t start = 0;
            size_t end = line.find_first_of(' ', start);
            while(end != std::string::npos) {
                genes.push_back(line.substr(start, end - start));
                start = end + 1;
                end = line.find_first_of(' ', start);
            }
            genes.push_back(line.substr(start));
            num_genes += genes.size();
            for (size_t k =0; k < genes.size(); k++) {
                gene_names.push_back(genes[k]);
            }
            if(useRC) { // add RC genes
                for (size_t k = genes.size() ; k > 0; k--) {
                    gene_names.push_back(genes[k-1]);
                }
            }
            // genes
            getline(ifs, line);
            if (!T.empty())
                T.push_back(IupacMask::DELIMITER);
            std::for_each(line.begin(), line.end(), [](char & c) { // replace invalid characters
                if (validCharacters.find(c) == validCharacters.end())
                    c = IupacMask::INVALID_REPLACEMENT;
            });
            T.append(line);
            stringStartPositions.push_back(T.size() + 1);
            if(useRC) {
                T.push_back(IupacMask::DELIMITER);
                T.append(Motif::ReverseComplement(line));
                stringStartPositions.push_back(T.size() + 1);
            }
            // std::cout << T << std::endl;

            // add gene start locations...
            std::vector<size_t> gene_sizes;
            start = 0;
            end = line.find_first_of(' ', start);
            while(end != std::string::npos) {
                gene_sizes.push_back(end + 1 - start);
                start = end + 1;
                end = line.find_first_of(' ', start);
            }
            gene_sizes.push_back(line.size() + 1 - start);
            for (size_t k =0; k < gene_sizes.size(); k++) {
                current_pos += gene_sizes[k];
                next_gene_locations.push_back(current_pos);
            }
            if(useRC) { // add RC genes
                for (size_t k = gene_sizes.size(); k > 0; k--) {
                    current_pos += gene_sizes[k - 1];
                    next_gene_locations.push_back(current_pos);
                }
            }
        }
        T.push_back(IupacMask::DELIMITER);
        T.push_back(IupacMask::DELIMITER);

        // std::cerr << "[" << name << "] " << N << " gene families and " << num_genes << " genes" << std::endl;
        // PROCESS DATA
        auto prevTime = startChrono();
        // std::cerr << T << std::endl;
        // std::cerr << "start positions: " << std::endl;
        // for (auto x : stringStartPositions)
            // std::cerr << x << std::endl;

        SuffixTree ST(T, name, useRC, stringStartPositions, gene_names, next_gene_locations, order_of_species_mapping, countBls ? &motif_to_blsvector_map : NULL, printRep);

        if (mode == MATCH) {
            int count = ST.matchIupacPatterns(ifs, bls, maxDegeneration, l.second, min_bls, type == ALIGNMENT_BASED);
            double elapsed = stopChrono(prevTime);
            std::cerr << "[" << name << "] " << count <<  " motifs located in " << elapsed << "s" << std::endl;
        } else if (mode == DISCOVERY) {
            int count = ST.printMotifs(l, alphabet, maxDegeneration, bls, type == ALIGNMENT_BASED, parquetOutput.empty() ? NULL : parquet_stream); // 0 == AB, 1 is AF
            size_t iteratorcount = ST.getMotifsIteratedCount();

            totalCount += count;
            double elapsed = stopChrono(prevTime);
            std::cerr << "[" << name << "] iterated over " << iteratorcount << " motifs" << std::endl;
            // std::cerr << "\33[2K\r[" << name << "] iterated over " << iteratorcount << " motifs" << std::endl; // clear beginning if progress is kept!
            std::cerr << "[" << name << "] counted " << count << " valid motifs in " << elapsed << "s" << std::endl;
        } else {
            std::cerr << "wrong mode given: " << mode << std::endl;
        }
    }
    if (mode == DISCOVERY) { // print out if counted version
        size_t unique_count = 0;
        auto prevTime = startChrono();
        motif_to_blsvector_map.recPrintAndDelete( unique_count, parquetOutput.empty() ? NULL : parquet_stream);

        double elapsed = stopChrono(prevTime);
        std::cerr << "total motifs counted: " << totalCount ;
        if(countBls) { std::cerr << " of which " << unique_count << " are unique [in " << elapsed << "s]"; }
        std::cerr << std::endl;

        if(!parquetOutput.empty()) {
            parquet_stream->EndRowGroup();
            delete parquet_stream;
            outfile->Close();
        }

    } else if (mode == MATCH) motif_to_blsvector_map.deleteRoot();

}
